import { Component, EventEmitter, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService, ToastService, ErrorService } from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'lib-dining-request-event-edit',
  templateUrl: './dining-request-event-edit.component.html',
  styles: [`

    .modal-dialog-50 {
      max-width: 50%;
      width: 50%;
    }
    .modal-dialog-30 {
      max-width: 30%;
      width: 30%;
    }
    .modal-dialog-100 {
      position: absolute;
      margin-top: 0rem;
      max-width: 100%;
      width: 100%;
      height: 100%;
    }
    .modal-dialog.modal-dialog-50 .modal-header button.close {
        top: -2rem;
        color: #3e515b;
        right: -1.5rem;
    }

    .modal-dialog.modal-dialog-100 .modal-header button.close {
      top: -2rem;
      color: #3e515b;
      right: -1.5rem;
    }
    .modal-dialog-100 .modal-content {
        border-radius: 0px;
        height: 100%;
    }
    `],
  encapsulation: ViewEncapsulation.None
})
export class DiningRequestEventEditComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  @Output() modelChange: EventEmitter<any> = new EventEmitter();

  private paramSubscription: Subscription;
  private formChangeSubscription: Subscription;
  @ViewChild('form') form: AdvancedFormComponent;
  public formConfig: string;
  public model: any;
  public actionModel: any;
  public event: any;
  public updateAction: any;
  public isLoading: boolean = true;
  public diningRequestActions: number;
  private validThrough: any;
  public isExtend: boolean = false;

  constructor(protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translate: TranslateService,
    private _loading: LoadingService,
    private _toastService: ToastService,
    private _errorService: ErrorService) {
    super(_router, _activatedRoute);
  }

  ngOnInit(): void {
    this._loading.showLoading();
    this.okButtonDisabled = true;
    this.paramSubscription = this._activatedRoute.params.subscribe(async params => {
      if (params && params.hasOwnProperty('id')) {
        this.event = params['id'];

        await this._context.model('DiningRequestEvents')
          .where('id')
          .equal(this.event)
          .expand('eventStatus')
          .getItem().then(async res => {
            this.model = res;
            this.validThrough = new Date(res.validThrough).getTime();

            const action = this._activatedRoute.snapshot.data.hasOwnProperty('action') && this._activatedRoute.snapshot.data.action;
            switch (action) {
              case ('edit'): {
                const totalActions = await this._context.model('DiningRequestActions')
                .select('count(id) as total')
                .where('diningRequestEvent').equal(this.model && this.model.id) 
                .getItem();
                this.model.totalActions = totalActions && totalActions.total;
                this.formConfig = 'DiningRequestEvents/edit';
                this.modalTitle = this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.TitleEdit');
                this.modalClass = 'modal-dialog-50';
                break;
              }
              case 'extend': {
                this.isExtend = true;
                // find all cards for this event where:
                // - active is true
                // - the student status is either active or declared (since in 'declared' status, the card remains active)
                // - the student's action is in CompletedActionStatus
                const diningCards = await this._context.model('StudentDiningCards')
                  .select('count(id) as total')
                  .where('student/studentStatus/alternateName').equal('active') // active or declared student keeps dining free card active=true 
                  .or('student/studentStatus/alternateName').equal('declared')
                  .prepare()
                  .and('active').equal(1) // 1 for true
                  .and('action/diningRequestEvent').equal(this.model && this.model.id)
                  .and('action/actionStatus/alternateName').equal('CompletedActionStatus') 
                  .take(-1)
                  .getItem();
                this.diningRequestActions = diningCards && diningCards.total;
                this.formConfig = 'DiningRequestEvents/extend';
                this.modalTitle = this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.TitleExtend');
                this.modalClass = 'modal-dialog-100';
                break;
              }
              case 'action': {
                if (params['action']) {
                  try {
                    this.updateAction = await this._context.model('UpdateDiningRequestEventActions')
                      .where('id')
                      .equal(params['action'])
                      .getItem();
                  } catch (err) {
                    this.close({ skipLocationChange: true });
                    console.log(err);
                    this._errorService.showError(err, {
                      continueLink: '.'
                    });
                  }
                }
                this.okButtonDisabled = false;
                this.modalTitle = this.updateAction && (this.updateAction.totalCards === this.updateAction.totalCardsAffected)
                ? this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.TitleActionsSucceeded')
                :
                this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.TitleActionsFailed');
                break;
              }
              case 'delete': {
                const diningCards = await this._context.model('DiningRequestActions')
                  .select('count(id) as total')
                  .where('diningRequestEvent')
                  .equal(this.model.id)
                  .and('studentDiningCard/active')
                  .equal(1)
                  .take(-1)
                  .getItem();
                this.okButtonDisabled = !!diningCards.total;
                this.formConfig = 'DiningRequestEvents/delete';
                this.modalTitle = this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.TitleDelete');
                this.modalClass = 'modal-dialog-30';
                break;
              }
            }
          }).catch(err => {
            console.log(err);
            return this._errorService.navigateToError(err);
          });
      } else {
        this.model = {};
        this.formConfig = 'DiningRequestEvents/new';
        this.modalTitle = this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.TitleNew');
        this.modalClass = 'modal-dialog-50';
      }
      this._loading.hideLoading();
      this.isLoading = false;
    });
  }

  ngAfterViewChecked() {
    this.formChangeSubscription = this.form && this.form.dataChange.subscribe(async (event) => {
      if (event) {
        this.model = event;
        const formState = await this.form.form.formioReady;
        if (this.form && !(formState).pristine === true) {
          this.okButtonDisabled = !this.form.form.formio.checkValidity(null, false, null, true);
        }
      }
    })
  }

  showMessage() {
    if (this.form && this.form.form && this.form.form.formio && this.form.form.formio.data && (this.form.form.formio.checkValidity(null, false, null, true))
      && (this.form.form.formio.data.validThrough && ((new Date(this.form.form.formio.data.validThrough)).getTime() !== this.validThrough))) {

      const message = this.diningRequestActions
        ?
        (this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.CardsMessage') + this.diningRequestActions + " " + this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.Cards') + this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.ProcessMessage'))
        :
        this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.EmptyCardsMessage');
      return message;
    } else {
      return null;
    }
  }

  ok(): Promise<any> {
    const action = this._activatedRoute.snapshot.data.hasOwnProperty('action') && this._activatedRoute.snapshot.data.action;
    switch (action) {
      case ('edit'):
      case ('add'): {
        this._loading.showLoading();
        let event: any = {};
        if (this.form.form.formio.data) {
          event = this.form.form.formio.data;
        }
        if (action === 'add' && !event.hasOwnProperty('hasConsent')) {
          Object.assign(event, { hasConsent: true })
        }
        return this._context.model(`DiningRequestEvents`).save(event)
          .then(() => {
            this._loading.hideLoading();
            this._toastService.clear();
            // show success toast message
            this._toastService.show(
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Edit.SuccessTitle'),
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Edit.SuccessMessage')
            );
            // close modal
            this.close({
              fragment: 'reload',
              skipLocationChange: true
            });
          }).catch(err => {
            this._loading.hideLoading();
            console.log(err);
            this._errorService.showError(err, {
              continueLink: '.'
            });
            this._toastService.clear();
            // show fail toast message
            this._toastService.show(
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Edit.FailTitle'),
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Edit.FailMessage')
            );
            // close modal without triggering a reload
            this.cancel();
          });
      }
      case 'extend': {
        let event: any = {};
        if (this.form.form.formio.data) {
          event = this.form.form.formio.data;
        }
        this.close({
          fragment: 'reload',
          skipLocationChange: true
        });
        this._context.model(`DiningRequestEvents/${this.model.id}/Extend`).save({ event })
          .then((res) => {
            if (res) {
              this._toastService.clear();
              // show success toast message
              this._toastService.show(
                this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.SuccessTitle'),
                this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.SuccessMessage')
              );
              // close modal
              this.close({
                fragment: 'reload',
                skipLocationChange: true
              });
            } else {
              this._toastService.clear();
              // show fail toast message
              this._toastService.show(
                this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.FailTitle'),
                this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.FailMessage')
              );
            }
          }).catch(err => {
            console.log(err);
            this._errorService.showError(err, {
              continueLink: '.'
            });
            this._toastService.clear();
            // show fail toast message
            this._toastService.show(
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.FailTitle'),
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Extend.FailMessage')
            );
            // close modal without triggering a reload
            this.cancel();
          });
        break;
      }
      case 'action': {
        this.close({
          skipLocationChange: true
        });
        break;
      }
      case 'delete': {
        // close modal
        this._loading.showLoading();
        this._context.model(`DiningRequestEvents/${this.model.id}/Remove`).save({})
          .then(() => {
            this._loading.hideLoading();
            this._toastService.clear();
            // show success toast message
            this._toastService.show(
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Delete.SuccessTitle'),
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Delete.SuccessMessage')
            );
            this.close({
              fragment: 'reload',
              skipLocationChange: true
            });
          }).catch(err => {
            console.log(err);
            this._errorService.showError(err, {
              continueLink: '.'
            });
            this._toastService.clear();
            // show fail toast message
            this._toastService.show(
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Delete.FailTitle'),
              this._translate.instant('UniversisDiningModule.DiningRequestEvent.Actions.Delete.FailMessage')
            );
            // close modal without triggering a reload
            this.cancel();
          });
        break;
      }
    }
  }

  cancel(): Promise<any> {
    return this.close({ skipLocationChange: true });
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }
}


