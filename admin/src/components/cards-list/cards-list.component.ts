import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { AdvancedRowActionComponent, AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Subscription, Observable } from 'rxjs';
import { AngularDataContext } from '@themost/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { UserActivityService, AppEventService, LoadingService, ModalService, ErrorService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ClientDataQueryable } from '@themost/client';

@Component({
  selector: 'dining-card-list',
  templateUrl: './cards-list.component.html'
})
export class CardsListComponent implements OnInit, OnDestroy, AfterViewInit {

  public recordsTotal: any;
  private dataSubscription?: Subscription;
  private changeSubscription?: Subscription;
  @Input() title;
  @Input() tableConfiguration?: any;
  @Input() searchConfiguration?: any;
  @ViewChild('table') table?: AdvancedTableComponent;
  @ViewChild('search') search?: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch?: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  selectedItems!: any[];
  public canEdit : boolean = true;
    

  constructor(private _context: AngularDataContext,
    private _router: Router,
    private _activatedTable: ActivatedTableService,
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService) { }


    ngOnInit(): void {
      //
    }

    ngAfterViewInit(): void {
      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
          try {
              this._activatedTable.activeTable = this.table;
              this.searchConfiguration = data.searchConfiguration;
              if (data.tableConfiguration) {
                  // set config
                  this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
                  // reset search text
                  this.advancedSearch.text = "";
  
                  // reset table
                  this.table.reset(true);
              }
          } catch (err) {
              this._errorService.navigateToError(err);
          }
  
          this._userActivityService.setItem({
              category: this._translateService.instant('NewRequestTemplates.DiningRequestAction.Title'),
              description: this._translateService.instant('List'),
              url: window.location.hash.substring(1), // get the path after the hash
              dateCreated: new Date
          });
      });
  
      this.changeSubscription = this._appEvent.change.subscribe((event) => {
          if (this.table?.dataTable == null) {
              return;
          }
          if (event && event.target && event.model === 'StudentDiningCards') {
              this.table.fetchOne({
                  result: event.target.id
              });
          }
          if (event && event.target && event.model === this.table?.config?.model) {
              this.table.fetchOne({
                  id: event.target.id
              });
          }
      });
      
  }  


  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [{}];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'active as active', 'student/id as student', 'academicYear/id as academicYear', 'academicPeriod/id as academicPeriod'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
                  if (this.table) {
                    return this.table.unselected.findIndex((x) => {
                      return x.id === item.id;
                    }) < 0;
                  }
              });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              active: item.active,
              student: item.student,
              academicYear: item.academicYear,
              academicPeriod: item.academicPeriod
            };
            
          });
        }
      }
    }
    return items;
  }

  executeSuspendCard(active: boolean) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // get user's cancelReason
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              active: active,
              student: item.student, 
              academicYear: item.academicYear,
              academicPeriod: item.academicPeriod,
              dateCancelled: new Date(),
              cancelReason: data['cancelReason']
            };
            await this._context.model(this.table?.config?.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table?.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  executeReactivateCard(active: boolean) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              active: active,
              student: item.student, 
              academicYear: item.academicYear,
              academicPeriod: item.academicPeriod,
              dateCancelled: null,
              cancelReason: null
            };
            await this._context.model(this.table?.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table?.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async suspendAction() {
    try {
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: any) => {
        return item.active === true;
      });
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems, // set items
          modalTitle: this._translateService.instant('UniversisDiningModule.SuspendAction.Title'), // set title
          description: this._translateService.instant('UniversisDiningModule.SuspendAction.Description'), // set description
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'StudentDiningCards/massSuspendCard' : null,
          refresh: this.refreshAction, // refresh action event
          execute: this.executeSuspendCard(false)
        }
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  
  async reactivateAction() {
    try {
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: any) => {
        return item.active === false;
      });
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems, // set items
          modalTitle: this._translateService.instant('UniversisDiningModule.ReactivateAction.Title'), // set title
          description: this._translateService.instant('UniversisDiningModule.ReactivateAction.Description'), // set description
          refresh: this.refreshAction, // refresh action event
          execute: this.executeReactivateCard(true)
        }
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }


  selectReport(){
    this._router.navigate( [{ outlets:{ modal:'print'} }], {
      relativeTo: this._activatedRoute
    });
  }

}
