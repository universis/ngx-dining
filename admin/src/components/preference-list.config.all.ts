export const PREFERENCE_LIST_CONFIG_ALL = {
  "title": "DiningPreferences.Active",
  "model": "DiningRequestPreferences",
  "searchExpression": "(indexof(student/person/familyName, '${text}') ge 0 or indexof(student/person/givenName, '${text}') ge 0 or indexof(student/studentIdentifier, '${text}') ge 0 or indexof(student/uniqueIdentifier, '${text}') ge 0)",
  "selectable": false,
  "multipleSelect": false,
  "columns": [
    {
      "name": "id",
      "property": "id",
      "hidden": true
    },
    {
      "name": "actionId",
      "property": "actionId",
      "title": "NewRequestTemplates.DiningRequestAction.Request",
      "className": "text-center",
      formatters: [
        {
          "formatter": "ButtonFormatter",
          "formatOptions": {
            "buttonContent": "<i class=\"far fa-eye text-indigo\"></i>",
            "buttonClass": "btn btn-default",
            "commands": [
              "../../../requests/list/all",
              {
                "outlets": {
                  "modal": ["item", "${actionId}", "edit"]
                }
              }
            ]
          }
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-center": "${actionId != null}",
              "d-none": "${actionId == null}"
            }
          }
        }
      ],
      "virtual": true,
    },
    {
      "name": "action/actionStatus/alternateName",
      "property": "actionStatus",
      "title": "Requests.ActionStatusTitle",
      "formatters": [
        {
          "formatter": "TemplateFormatter",
          "formatString": "${actionStatus == null ? 'Unknown' : actionStatus}"
        },
        {
          "formatter": "TranslationFormatter",
          "formatString": "ActionStatusTypes.${value}"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}' && ('${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus')",
              "text-warning": "'${actionStatus}' && ('${actionStatus}'==='ActiveActionStatus')",
              "text-success": "'${actionStatus}' && ('${actionStatus}'==='CompletedActionStatus')"
            }
          }
        }
      ]
    },
    {
      "name": "diningRequestEvent/academicYear/alternateName",
      "property": "eventAcademicYear",
      "title": "UniversisDiningModule.DiningCardsTemplates.AcademicYear"
    },
    {
      "name": "student/person/familyName",
      "property": "familyName",
      "title": "Students.FullName",
      "formatter": "TemplateFormatter",
      "formatString": "${familyName} ${givenName}"
    },
    {
      "name": "student/person/givenName",
      "property": "givenName",
      "title": "Students.GivenName",
      "hidden": true
    },
    {
      "name": "student/person/fatherName",
      "property": "studentFatherName",
      "title": "Requests.FatherName"
    },
    {
      "name": "student/person/motherName",
      "property": "studentMotherName",
      "title": "Requests.MotherName"
    },
    {
      "name": "student/studentStatus/alternateName",
      "property": "studentStatus",
      "title": "Requests.StudentStatus",
      "formatter": "TranslationFormatter",
      "formatString": "StudentStatuses.${value}"
    },
    {
      "name": "student/semester",
      "property": "semester",
      "title": "Students.Semester"
    },
    {
      "name": "student/department/name",
      "property": "studentDepartmentName",
      "title": "Students.Department"
    },
    {
      "name": "student/studentIdentifier",
      "property": "studentIdentifier",
      "title": "Students.StudentIdentifier",
      "className": "text-right"
    },
    {
      "name": "student/uniqueIdentifier",
      "property": "studentUniqueIdentifier",
      "title": "Requests.StudentUniqueIdentifier"
    },
    {
      "name": "student",
      "property": "student",
      "title": "Students.StudentIdentifier",
      "formatter": "TemplateFormatter",
      "hidden": true
    },
    {
      "name": "action/diningRequestEvent/eventStatus/alternateName",
      "property": "eventStatus",
      "title": "DiningRequestEvent.Status",
      "hidden": true
    },
    {
      "name": "action/id",
      "property": "actionId",
      "title": "DiningRequestActionAction",
      "hidden": true
    }
  ],
  "defaults": {
    "filter": "(preference eq true)",
    "expand": "student, action"
  },
  "criteria": [
    {
      "name": "studentUniqueIdentifier",
      "filter": "(indexof(student/uniqueIdentifier, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentName",
      "filter": "(indexof(student/person/familyName, '${value}') ge 0 or indexof(student/person/givenName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "studentActiveStatus",
      "filter": "${value >=1 ? '(student/studentStatus/alternateName eq \\'active\\')' : '(student/studentStatus/alternateName ne \\'active\\')'}",
      "type": "text"
    },
    {
      "name": "studentDepartment",
      "filter": "(student/department/id eq '${value}')",
      "type": "text"
    },
    {
      "name": "eventAcademicYear",
      "filter": "(indexof(diningRequestEvent/academicYear/alternateName,'${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "location",
      "filter": "(location eq '${value}')",
      "type": "text"
    },
    {
      "name": "requestNumber",
      "filter": "(indexof(action/requestNumber, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "eventStatus",
      "filter": "(action/diningRequestEvent/eventStatus/alternateName eq '${value}')",
      "title": "DiningRequestEvent.Status"
    },
    {
      "name": "eventAcademicYear",
      "filter": "(indexof(action/diningRequestEvent/academicYear/alternateName, '${value}') ge 0)",
      "type": "text"
    },
    {
      "name": "semester",
      "filter": "(student/semester eq '${value}')",
      "type": "text"
    },
    {
      "name": "semesterLowerOrEqual",
      "filter": "(student/semester le ${value})",
      "type": "text"
    },
    {
      "name": "studentHasAction",
      "filter": "${value >=1 ? '(action ne null)' : '(action eq null)'}",
      "type": "text"
    },
    {
      "name": "studentIdentifier",
      "filter": "(indexof(student/studentIdentifier, '${value}') ge 0)",
      "type": "text"
    }
  ],
  "paths": [
    {
      "name": "UniversisDiningModule.DiningRequestPreference.Active",
      "show": true,
      "alternateName": "list/active"
    },
    {
      "name": "UniversisDiningModule.DiningRequestPreference.All",
      "show": true,
      "alternateName": "list/all"
    }
  ],
  "searches": []
}