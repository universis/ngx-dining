export const UPDATE_ACTIONS_SEARCH_CONFIG_RESULTS = {
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "UniversisDiningModule.DiningCardsTemplates.AcademicYear",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "AcademicYears?$top={{limit}}&$skip={{skip}}&$orderby=id desc",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "<span class='d-block pr-5 text-truncate'>{{ item.name }}</span>",
              "key": "academicYear",
              "valueProperty": "alternateName",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        }, 
        {
          "components": [
            {
              "label": "UniversisDiningModule.DiningRequestEvent.ActionStatus",
              "widget": "choicesjs",
              "dataSrc": "url",
              "data": {
                "url": "ActionStatusTypes?$filter=(alternateName ne 'PotentialActionStatus' and alternateName ne 'CancelledActionStatus')&$orderby=alternateName",
                "headers": []
              },
              "selectThreshold": 0.3,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "template": "{{ item.alternateName }}",
              "key": "actionStatus",
              "valueProperty": "alternateName",
              "selectValues": "value",
              "type": "select",
              "input": true,
              "hideOnChildrenHidden": false,
              "disableLimit": false,
              "lazyLoad": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    },
    {
      "label": "Columns",
      "columns": [
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    }
  ]
}
