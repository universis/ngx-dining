export const PREFERENCE_SEARCH_CONFIG_ACTIVE = {
  "components": [
    {
      "label": "Columns",
      "columns": [
        {
          "components": [
            {
              "label": "UniversisDiningModule.DiningRequestPreference.StudentHasAction",
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentHasAction",
              "input": true,
              "searchEnabled": false,
              "hideOnChildrenHidden": false,
              "widget": "choicesjs",
              "type": "select",
              "selectThreshold": 0.3,
              "valueProperty": "value",
              "selectValues": "value",
              "data": {
                  "values": [                    
                      {
                        "value": 1,
                        "label": "Yes"
                      },
                      {
                        "value": -1,
                        "label": "No"
                      }
                  ]
              }
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.StudentUniqueIdentifier",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentUniqueIdentifier",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.FullName",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentName",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 6,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.StudentStatus",
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentActiveStatus",
              "input": true,
              "searchEnabled": false,
              "hideOnChildrenHidden": false,
              "widget": "choicesjs",
              "type": "select",
              "selectThreshold": 0.3,
              "valueProperty": "value",
              "selectValues": "value",
              "data": {
                "values": [
                  {
                    "value": 1,
                    "label": "StudentStatuses.active"
                  },
                  {
                    "value": -1,
                    "label": "StudentStatuses.NotActive"
                  }
                ]
              }
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Students.StudentDepartment",
              "labelPosition": "top",
              "widget": "choicesjs",
              "key": "studentDepartment",
              "dataSrc": "url",
              "data": {
                "url": "LocalDepartments?$top=-1&$skip=0&$orderby=name",
                "headers": []
              },
              "template": " {{item.id}} - {{item.name}} ",
              "selectValues": "value",
              "valueProperty": "id",
              "type": "select",
              "input": true,
              "disabled": false,
              "lazyLoad": false,
              "searchEnabled": true
            }
          ],
          "width": 4,
          "offset": 0,
          "push": 0,
          "pull": 0
        }, 
        {
          "components": [
            {
              "label": "Students.StudentIdentifier",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "studentIdentifier",
              "type": "textfield",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 2,
          "offset": 0,
          "push": 0,
          "pull": 0
        },
        {
          "components": [
            {
              "label": "Requests.SemesterLowerOrEqual",
              "spellcheck": true,
              "tableView": true,
              "validate": {
                "unique": false,
                "multiple": false
              },
              "key": "semesterLowerOrEqual",
              "type": "number",
              "input": true,
              "hideOnChildrenHidden": false
            }
          ],
          "width": 3,
          "offset": 0,
          "push": 0,
          "pull": 0
        }
      ],
      "tableView": false,
      "key": "columns1",
      "type": "columns",
      "input": false,
      "path": "columns1"
    }
  ]
}
