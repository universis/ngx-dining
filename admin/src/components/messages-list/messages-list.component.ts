import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { AdvancedTableComponent, AdvancedSearchFormComponent, AdvancedTableSearchComponent, ActivatedTableService, AdvancedTableDataResult, AdvancedTableConfiguration } from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { UserActivityService, AppEventService, ErrorService } from '@universis/common';


@Component({
  selector: 'lib-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.css']
})


export class MessagesListComponent implements OnInit, AfterViewInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription?: Subscription;
  private changeSubscription?: Subscription;
  @Input() title;
  @Input() tableConfiguration?: any;
  @Input() searchConfiguration?: any;
  @ViewChild('table') table?: AdvancedTableComponent;
  @ViewChild('search') search?: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch?: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  selectedItems!: any[];


  constructor(
    private _activatedTable: ActivatedTableService,
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _errorService: ErrorService
  ) { }


  ngOnInit(): void {
    //
  }

  ngAfterViewInit(): void {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      try {
        this._activatedTable.activeTable = this.table;
        this.searchConfiguration = data.searchConfiguration;
        if (data.tableConfiguration) {
          // set config
          this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
          // reset search text
          this.advancedSearch.text = "";

          // reset table
          this.table.reset(true);
        }
      } catch (err) {
        console.log(err);
        this._errorService.navigateToError(err);
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('NewRequestTemplates.DiningRequestAction.Title'),
        description: this._translateService.instant('List'),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      if (this.table?.dataTable == null) {
        return;
      }
      if (event && event.target && event.model === 'DiningRequestActionMessages') {
        this.table.fetchOne({
          result: event.target.id
        });
      }
      if (event && event.target && event.model === this.table?.config?.model) {
        this.table.fetchOne({
          id: event.target.id
        });
      }
    });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }
}
