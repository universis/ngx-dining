import { TableConfiguration } from '@universis/ngx-tables';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


import { PREFERENCE_LIST_CONFIG_ALL } from './preference-list.config.all';
import { PREFERENCE_SEARCH_CONFIG_ALL } from './preference-search.config.all';
import { PREFERENCE_LIST_CONFIG_ACTIVE } from './preference-list.config.active'
import { PREFERENCE_SEARCH_CONFIG_ACTIVE } from './preference-search.config.active'
export class PreferenceTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'active') {
                return PREFERENCE_LIST_CONFIG_ACTIVE;
            }
            else if (route.params.list == 'all') {
                return PREFERENCE_LIST_CONFIG_ALL;
            }
        } catch (err) {
            return PREFERENCE_LIST_CONFIG_ACTIVE;
        }
    }
}

export class PreferenceTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'active') {
                return PREFERENCE_SEARCH_CONFIG_ACTIVE;
            }
            else if (route.params.list == 'all') {
                return PREFERENCE_SEARCH_CONFIG_ALL;
            }
        } catch (err) {
            return PREFERENCE_SEARCH_CONFIG_ACTIVE;
        }
    }
}

export class PreferenceDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return PREFERENCE_LIST_CONFIG_ACTIVE;
    }
}