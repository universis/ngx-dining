import { Component, OnInit, ViewChild, Input, Output, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { AdvancedRowActionComponent, AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Subscription, Observable } from 'rxjs';
import { AngularDataContext } from '@themost/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { UserActivityService, AppEventService, LoadingService, ModalService, ErrorService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ClientDataQueryable } from '@themost/client';
import { SendMessageActionComponent } from '@universis/ngx-dining';

@Component({
  selector: 'dining-request-action-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit, OnDestroy, AfterViewInit {


private dataSubscription?: Subscription;
private changeSubscription?: Subscription;

@Input() tableConfiguration: any;
@Input() searchConfiguration: any;
@Input() title: any;
@Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

@ViewChild('table') table?: AdvancedTableComponent;
@ViewChild('search') search?: AdvancedSearchFormComponent;
@ViewChild('advancedSearch') advancedSearch?: AdvancedTableSearchComponent;

public recordsTotal: any;
selectedItems: any[]=[];


  constructor(private _context: AngularDataContext,
    private _router: Router,
    private _activatedTable: ActivatedTableService,
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService) { }
    
    ngOnInit(): void {
        //
    }
  
    ngAfterViewInit() {
      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
          try {
              this._activatedTable.activeTable = this.table;
              this.searchConfiguration = data.searchConfiguration;
              if (data.tableConfiguration) {
                  // set config
                  this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
                  // reset search text
                  this.advancedSearch.text = "";
  
                  // reset table
                  this.table.reset(true);
              }
          } catch (err) {
              this._errorService.navigateToError(err);
          }
  
          this._userActivityService.setItem({
              category: this._translateService.instant('NewRequestTemplates.DiningRequestAction.Title'),
              description: this._translateService.instant('List'),
              url: window.location.hash.substring(1), // get the path after the hash
              dateCreated: new Date
          });
      });
  
      this.changeSubscription = this._appEvent.change.subscribe((event) => {
          if (this.table?.dataTable == null) {
              return;
          }
          if (event && event.target && event.model === 'DininRequestActions') {
              this.table.fetchOne({
                  result: event.target.id
              });
          }
          if (event && event.target && event.model === this.table?.config?.model) {
              this.table.fetchOne({
                  id: event.target.id
              });
          }
      });
  
    }  
    
  accept() {
    //
  }

  reject() {

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [{}];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus', 'owner', 'student/id as student'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
                items = queryItems.filter(item => {
                  if (this.table) {
                  return this.table.unselected.findIndex((x) => {
                    return x.id === item.id;
                  }) < 0;
                }
              }
            );
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            // set recipient~student(which is this action owner)
            return {
              id: item.id,
              actionStatus: item.actionStatus,
              owner: item.owner,
              student: item.studentId
            };
          });
        }
      }
    }
    return items;
  }

  executeChangeActionStatus(statusText: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              }
            };
            await this._context.model(this.table?.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table?.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executeSendDirectMessageAction(message) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const newMessage = Object.assign({}, message, {
              recipient: item.owner,
              action: item.id,
              student: item.student
            });
            await this._context.model(`DiningRequestActions/${item.id}/messages`).save(newMessage);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async sendDirectMessageAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      const message = {};
      this._modalService.openModalComponent(SendMessageActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Register.ComposeNewMessage.Title',
          description: 'Register.ComposeNewMessage.Description',
          refresh: this.refreshAction,
          message: message,
          execute: this.executeSendDirectMessageAction(message)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
    
  }

  async acceptAction() {
    try {
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: any) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems, // set items
          modalTitle: this._translateService.instant('UniversisDiningModule.AcceptAction.Title'), // set title
          description: this._translateService.instant('UniversisDiningModule.AcceptAction.Description'), // set description
          refresh: this.refreshAction, // refresh action event
          execute: this.executeChangeActionStatus('CompletedActionStatus')
        }
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  
  async rejectAction() {
    try {
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: any) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems, // set items
          modalTitle: this._translateService.instant('UniversisDiningModule.RejectAction.Title'), // set title
          description: this._translateService.instant('UniversisDiningModule.RejectAction.Description'), // set description
          refresh: this.refreshAction, // refresh action event
          execute: this.executeChangeActionStatus('CancelledActionStatus')
        }
      });
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

}
