export const UPDATE_ACTIONS_LIST_CONFIG_RESULTS = {
  $schema: "https://universis.gitlab.io/ngx-tables/schemas/v1/schema.json",
  model: "UpdateDiningRequesteventActions",
  // tslint:disable-next-line: max-line-length
  searchExpression: "((object/id eq '${Number(text)}') or (indexof(object/academicYear/name, '${text}') ge 0))",
  columns: [
    {
      name: "object/id",
      property: "eventId",
      title: "UniversisDiningModule.DiningRequestEvent.Id"
    },
    {
      name: "id",
      property: "actionId",
      title: "UniversisDiningModule.DiningRequestEvent.Id",
      hidden: true
    },
    {
      name: "object/academicYear/name",
      property: "academicYear",
      title: "UniversisDiningModule.DiningRequestEvent.AcademicYear"
    },
    {
      name: "description",
      property: "description",
      title: "UniversisDiningModule.DiningRequestEvent.ActionDescription"
    },
    {
      name: "currentEndDate",
      property: "currentEndDate",
      title: "UniversisDiningModule.DiningRequestEvent.EndDateBefore",
      formatter: "DateTimeFormatter",
      formatString: "short",
      formatters: [
        {
          formatter: "DateTimeFormatter",
          formatString: "short"
        }
      ],
      virtual: true
    },
    {
      name: "currentEndDate",
      property: "currentEndDate",
      title: "UniversisDiningModule.DiningRequestEvent.EndDateBefore",
      hidden: true
    },
    {
      name: "endDate",
      property: "endDate",
      title: "UniversisDiningModule.DiningRequestEvent.EndDateAfter",
      formatter: "DateTimeFormatter",
      formatString: "short",
      virtual: true
    },
    {
      name: "endDate",
      property: "endDate",
      title: "UniversisDiningModule.DiningRequestEvent.EndDateAfter",
      hidden: true
    },
    {
      name: "currentValidThrough",
      property: "currentValidThrough",
      title: "UniversisDiningModule.DiningRequestEvent.ValidThroughBefore",
      formatter: "DateTimeFormatter",
      formatString: "short",
      virtual: true
    },
    {
      name: "currentValidThrough",
      property: "currentValidThrough",
      title: "UniversisDiningModule.DiningRequestEvent.ValidThroughBefore",
      hidden: true
    },
    {
      name: "validThrough",
      property: "validThrough",
      title: "UniversisDiningModule.DiningRequestEvent.ValidThroughAfter",
      formatter: "DateTimeFormatter",
      formatString: "short",
      virtual: true
    },
    {
      name: "validThrough",
      property: "validThrough",
      title: "UniversisDiningModule.DiningRequestEvent.ValidThroughAfter",
      hidden: true
    },
    {
      name: "totalCards",
      property: "totalCards",
      title: "UniversisDiningModule.DiningRequestEvent.CardsScheduled"
    },
    {
      name: "totalCardsAffected",
      property: "totalCardsAffected",
      title: "UniversisDiningModule.DiningRequestEvent.CardsSucceeded",
    },
    {
      name: "totalCards",
      property: "totalCards",
      virtual: true,
      title: "UniversisDiningModule.DiningRequestEvent.CardsFailed",
      formatter: "TemplateFormatter",
      formatString: "${ ((validThrough != null) && (currentValidThrough != null) && totalCards !== null) ? (totalCards - totalCardsAffected) : null}"
    },
    {
      name: "dateCreated",
      property: "dateCreated",
      title: "UniversisDiningModule.DiningRequestEvent.DateCreatedAction",
      formatter: "DateTimeFormatter",
      formatString: "short",
      virtual: true
    },
    {
      name: "dateCreated",
      property: "dateCreated",
      title: "UniversisDiningModule.DiningRequestEvent.DateCreated",
      hidden: true
    },
    {
      name: "createdBy/alternateName",
      property: "createdBy",
      title: "UniversisDiningModule.DiningRequestEvent.CreatedBy",
    },
    {
      name: "actionStatus/alternateName",
      property: "actionStatus",
      title: "UniversisDiningModule.DiningRequestEvent.ActionStatus",
      formatters: [
        {
          formatter: "TranslationFormatter",
          formatString: "UpdateActionStatusTypes.${value}"
        },
        {
          formatter: "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-danger": "'${actionStatus}'==='CancelledActionStatus' || '${actionStatus}'==='FailedActionStatus'",
              "text-warning": "'${actionStatus}'==='ActiveActionStatus'",
              "text-success": "'${actionStatus}'==='CompletedActionStatus'"
            }
          }
        }
      ]
    },
    {
      name: "actionResult",
      property: "actionResult",
      hidden: true
    },
    {
      name: "id",
      property: "id",
      title: "UniversisDiningModule.DiningRequestEvent.FailureReasonTableTitle",
      formatters: [
        {
          "formatter": "ButtonFormatter",
          "formatString": "#/dining/admin-page/list/results/(modal:item/${eventId}/action/${id}",
          "formatOptions": {
            "buttonTitle": "UniversisDiningModule.DiningRequestEvent.FailureReason",
            "buttonText": "UniversisDiningModule.DiningRequestEvent.FailureReason",
            "buttonClass": "btn btn-default text-theme"
          }
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "d-none": "${!(actionStatus === 'FailedActionStatus')}"
            }
          }
        }
      ]
    }
  ],
  defaults: {
    orderBy: "dateCreated desc"
  },
  paths: [
  ],
  criteria: [
    {
      name: "academicYear",
      filter: "(object/academicYear/alternateName eq '${value}')",
      type: "text"
    },
    {
      name: "actionStatus",
      filter: "(actionStatus/alternateName eq '${value}')",
      type: "text"
    }
  ],
  searches: []
};
