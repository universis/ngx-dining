import { TableConfiguration } from '@universis/ngx-tables';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';


import { EVENT_LIST_CONFIG_ALL } from './event-list.config.events';
import { EVENT_SEARCH_CONFIG_ALL } from './event-search.config.events';
import { UPDATE_ACTIONS_LIST_CONFIG_RESULTS } from './update-actions-list.config.results';
import { UPDATE_ACTIONS_SEARCH_CONFIG_RESULTS } from './update-actions-search.config.results';

export class EventsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'events') {
                return EVENT_LIST_CONFIG_ALL;
            }
            else if (route.params.list == 'results') {
                return UPDATE_ACTIONS_LIST_CONFIG_RESULTS;
            }
        } catch (err) {
            console.log(err);
            return EVENT_LIST_CONFIG_ALL;
        }
    }
}

export class EventsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration | any {
        try {
            if (route.params.list == 'events') {
                return EVENT_SEARCH_CONFIG_ALL;
            }
            else if (route.params.list == 'results') {
                return UPDATE_ACTIONS_SEARCH_CONFIG_RESULTS;
            }
        } catch (err) {
            console.log(err);
            return EVENT_SEARCH_CONFIG_ALL;
        }
    }
}
