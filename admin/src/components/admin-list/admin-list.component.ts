import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ClientDataQueryable } from '@themost/client';
import { UserActivityService, AppEventService, ModalService, LoadingService, ErrorService } from '@universis/common';
import { AdvancedTableComponent, AdvancedSearchFormComponent, AdvancedTableSearchComponent, ActivatedTableService, AdvancedTableConfiguration, AdvancedTableDataResult, AdvancedRowActionComponent } from '@universis/ngx-tables';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'lib-admin-list',
  templateUrl: './admin-list.component.html'
})
export class AdminListComponent implements OnInit, OnDestroy, AfterViewInit {

  public recordsTotal: any;
  private dataSubscription?: Subscription;
  private paramSubscription?: Subscription;
  @Input() title;
  @Input() tableConfiguration?: any;
  @Input() searchConfiguration?: any;
  @ViewChild('table') table?: AdvancedTableComponent;
  @ViewChild('search') search?: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch?: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  selectedItems!: any[];
  public modelName: any;

  constructor(private _context: AngularDataContext,
    private _router: Router,
    private _activatedTable: ActivatedTableService,
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _errorService: ErrorService) { }


    ngOnInit(): void {
    //
    }

    ngAfterViewInit(): void {
      this.loadData();
      this.paramSubscription = this._activatedRoute.fragment.subscribe((data)=> { 
        if (data === 'reload') {
          this.loadData();
        }
      });
  }


  loadData() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      try {
          this._activatedTable.activeTable = this.table;
          this.searchConfiguration = data.searchConfiguration;
          if (data.tableConfiguration) {
              // set config
              this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
              this.modelName = this.table.config.model;
              // reset search text
              this.advancedSearch.text = "";
              // reset table
              this.table.reset(true);
          }
      } catch (err) {
        console.log(err);
        this._errorService.navigateToError(err);
      }

      this._userActivityService.setItem({
          category: this._translateService.instant('NewRequestTemplates.UniversisDiningModule.DiningRequestEvent.Description'),
          description: this._translateService.instant('List'),
          url: window.location.hash.substring(1), // get the path after the hash
          dateCreated: new Date
      });
  });

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
  
}
