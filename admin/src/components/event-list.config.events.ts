export const EVENT_LIST_CONFIG_ALL = {
  title: "DiningEvents.All",
  model: "DiningRequestEvents",
  searchExpression: "(indexof(academicYear/alternateName, '${text}') ge 0)",
  columns: [
    {
      name: "id",
      property: "linkID",
      formatter: "ActionLinkFormatter",
      actions: [
        {
          title: "UniversisDiningModule.DiningRequestEvent.Edit",
          formatOptions: {
            commands: [
              {
                outlets: {
                  modal: ["item", "${encodeURIComponent(id)}", "edit"],
                },
              },
            ],
          },
        },
        {
          title: "UniversisDiningModule.DiningRequestEvent.Extend",
          formatOptions: {
            commands: [
              {
                outlets: {
                  modal: ["item", "${encodeURIComponent(id)}", "extend"],
                },
              },
            ],
          },
        },
        {
          title: "UniversisDiningModule.DiningRequestEvent.Delete",
          formatOptions: {
            commands: [
              {
                outlets: {
                  modal: ["item", "${encodeURIComponent(id)}", "delete"],
                },
              },
            ],
          },
        },
      ],
    },
    {
      name: "id",
      property: "id",
      title: "UniversisDiningModule.DiningRequestEvent.ID",
    },
    {
      name: "academicYear/alternateName",
      property: "academicYear",
      title: "UniversisDiningModule.DiningRequestEvent.AcademicYear",
    },
    {
      name: "startDate",
      title: "UniversisDiningModule.DiningRequestEvent.StartDate",
      filter: "(startDate eq '${value}')",
      type: "text",
      formatter: "DateTimeFormatter",
      formatString: "shortDate",
    },
    {
      name: "endDate",
      title: "UniversisDiningModule.DiningRequestEvent.EndDate",
      filter: "(endDate eq '${value}')",
      type: "text",
      formatter: "DateTimeFormatter",
      formatString: "shortDate",
    },
    {
      name: "validFrom",
      title: "UniversisDiningModule.DiningRequestEvent.ValidFrom",
      filter: "(validFrom eq '${value}')",
      type: "text",
      formatter: "DateTimeFormatter",
      formatString: "shortDate",
    },
    {
      name: "validThrough",
      title: "UniversisDiningModule.DiningRequestEvent.ValidThrough",
      filter: "(validThrough eq '${value}')",
      type: "text",
      formatter: "DateTimeFormatter",
      formatString: "shortDate",
    },
    {
      name: "hasConsent",
      property: "hasConsent",
      title:"UniversisDiningModule.DiningRequestEvent.RequiredPrivacyPolicyConsent",
			className: "text-center",
      formatters: [
        {
          "formatter": "TrueFalseFormatter"
        },
        {
          "formatter": "NgClassFormatter",
          "formatOptions": {
            "ngClass": {
              "text-success": "${hasConsent}",
              "text-danger": "${!hasConsent}"
            }
          }
        }
      ]
    },
    {
      name: "eventStatus/alternateName",
      property: "eventStatus",
      title: "UniversisDiningModule.DiningRequestEvent.Status",
			className: "text-center",
      formatters: [
        {
          formatter: "TranslationFormatter",
          formatString:
            "UniversisDiningModule.DiningRequestEvent.EventStatusTypes.${value}",
        },
        {
          formatter: "NgClassFormatter",
          formatOptions: {
            ngClass: {
              "text-danger": "'${eventStatus}'==='EventCancelled'",
              "text-info":
                "'${eventStatus}'==='EventPostponed' || '${eventStatus}'==='EventRescheduled' || '${eventStatus}'==='EventScheduled'",
              "text-success":
                "'${eventStatus}'==='EventOpened' || '${eventStatus}'==='EventCompleted'",
            },
          },
        },
      ],
    },
  ],
  criteria: [
    {
      name: "academicYear",
      filter: "(academicYear/alternateName eq '${value}')",
      type: "text",
    },
    {
      name: "eventStatusOpened",
      filter: "${value >=1 ? '(eventStatus/alternateName eq \\'EventOpened\\')' : '(eventStatus/alternateName ne \\'EventOpened\\')'}", 
      type: "text",
    }
  ],
  defaults: {
    orderBy: "academicYear desc",
  },
  paths: [
    {
      name: "UniversisDiningModule.DiningRequestEvent.All",
      show: true,
      alternateName: "list/events",
    },
    {
      name: "UniversisDiningModule.DiningRequestEvent.Actions.TitleActionsFailed",
      show: true,
      alternateName: "list/results",
    }
  ],
  searches: [],
};
