import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdvancedFormItemResolver, AdvancedFormModalData } from '@universis/forms';
// import { AdvancedListComponent } from '@universis/ngx-tables';
import { CardsListComponent } from './components/cards-list/cards-list.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModalEditCardComponent } from './components/edit-card/modal-edit-card/modal-edit-card.component';
import { EditComponent } from './components/edit/edit.component';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { ListComponent } from './components/list/list.component';
import { SelectReportComponent } from '@universis/ngx-reports';
import { DocumentSeriesResolverService } from './document-series-resolver.service';
import { DiningConfigurationResolver } from '@universis/ngx-dining/shared';
import { MessagesListComponent } from './components/messages-list/messages-list.component';
import { CardsHomeComponent } from './components/cards-home/cards-home.component';
import { RequestsHomeComponent } from './components/requests-home/requests-home.component';
import { RequestsTableConfigurationResolver, RequestsTableSearchResolver } from './components/requests-table-config.resolver';
import { CardsTableConfigurationResolver, CardsTableSearchResolver } from './components/cards-table-config.resolver';
import { PreferenceTableConfigurationResolver, PreferenceTableSearchResolver } from './components/preference-table-config.resolver';
import { PreferenceHomeComponent } from './components/preference-home/preference-home.component';
import { PreferenceListComponent } from './components/preference-list/preference-list.component';
import { MessagesHomeComponent } from './components/messages-home/messages-home.component';
import { MessagesTableConfigurationResolver, MessagesTableSearchResolver } from './components/messages-table-config.resolver';
import { AdminListComponent } from './components/admin-list/admin-list.component';
import { EventsTableConfigurationResolver, EventsTableSearchResolver } from './components/event-table-config.resolver';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { DiningRequestEventEditComponent} from './components/edit-event/dining-request-event-edit.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'requests'
  },
  {
    path: 'requests',
    component: RequestsHomeComponent,
    data: {
      title: "DiningRequests"
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list/:list',
        component: ListComponent,
        data: {
          model: 'DiningRequestActions',
          description: 'NewRequestTemplates.DiningRequestAction.Description'
        },
        resolve: {
          tableConfiguration: RequestsTableConfigurationResolver,
          searchConfiguration: RequestsTableSearchResolver
        },
        children: [
          {
            path: 'item/:id/edit',
            component: ModalEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiningRequestActions',
              action: 'modal-edit',
              serviceQueryParams: EditComponent.ServiceQueryParams,
              closeOnSubmit: true,
              continueLink: '.'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          },
        ]
      },
    ]
  },
  {
    path: 'cards',
    component: CardsHomeComponent,
    data: {
      title: "DiningCards"
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list/:list',
        component: CardsListComponent,
        data: {
          model: 'StudentDiningCards',
          description: 'UniversisDiningModule.DiningCardsTemplates.DiningCard.Description',
        },
        resolve: {
          tableConfiguration: CardsTableConfigurationResolver,
          searchConfiguration: CardsTableSearchResolver
        },
        children: [
          {
            path: 'item/:id/edit',
            component: ModalEditCardComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'StudentDiningCards',
              action: 'modal-edit',
              serviceQueryParams: EditCardComponent.ServiceQueryParams,
              closeOnSubmit: true,
              continueLink: '.'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          },
          {
            path: 'print',
            pathMatch: 'full',
            component: SelectReportComponent,
            outlet: 'modal',
            resolve: {
              item: DiningConfigurationResolver,
              documentSeriesUrl: DocumentSeriesResolverService
            },
            data: {
              model: 'DiningConfigurations'
            }
          }
        ]
      },
    ]
  },
  {
    path: 'messages',
    component: MessagesHomeComponent,
    data: {
      title: "Messages"
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list/:list',
        component: MessagesListComponent,
        data: {
          model: 'DiningRequestActionMessages',
          description: 'UniversisDiningModule.Messages.Title',
        },
        resolve: {
          tableConfiguration: MessagesTableConfigurationResolver,
          searchConfiguration: MessagesTableSearchResolver
        },
        children: [
          {
            path: 'item/:id/edit',
            component: ModalEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiningRequestActions',
              action: 'modal-edit',
              serviceQueryParams: EditComponent.ServiceQueryParams,
              closeOnSubmit: true,
              continueLink: '.'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          }
        ]
      },
    ]
  },
  {
    path: 'dining-preference',
    component: PreferenceHomeComponent,
    data: {
      title: "DiningRequestPreferences"
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list/:list',
        component: PreferenceListComponent,
        data: {
          model: 'DiningRequestPreferences',
          description: 'UniversisDiningModule.DiningCardsTemplates.DiningRequestPreference.Description',
        },
        resolve: {
          tableConfiguration: PreferenceTableConfigurationResolver,
          searchConfiguration: PreferenceTableSearchResolver
        }
      }
    ] 
  },
  {
    path: 'admin-page',
    component: AdminHomeComponent,
    data: {
      title: "DiningAdminPageTitle"
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/events'
      },
      {
        path: 'list/:list',
        component: AdminListComponent,
        data: {
          model: 'DiningRequestEvents',
          description: 'UniversisDiningModule.DiningRequestEvent.Description',
        },
        resolve: {
          tableConfiguration: EventsTableConfigurationResolver,
          searchConfiguration: EventsTableSearchResolver
        },
        children: [
          {
            path: 'add',
            component: DiningRequestEventEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'DiningRequestEvents',
              action: 'add',
              closeOnSubmit: true,
              continueLink: '/admin-page'
            },
            resolve: {
              formConfig: AdvancedFormItemResolver
            }
          },
          {
            path: 'item/:id/edit',
            component: DiningRequestEventEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiningRequestEvents',
              closeOnSubmit: true,
              action: "edit",
              continueLink: '/admin-page'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          },
          {
            path: 'item/:id/delete',
            component: DiningRequestEventEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiningRequestEvents',
              closeOnSubmit: true,
              action: "delete",
              continueLink: '/admin-page'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          },
          {
            path: 'item/:id/action/:action',
            component: DiningRequestEventEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              closeOnSubmit: true,
              action: "action",
              continueLink: '/admin-page', 
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          },
          {
            path: 'item/:id/extend',
            component: DiningRequestEventEditComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'DiningRequestEvents',
              closeOnSubmit: true,
              action: "extend",
              continueLink: '/admin-page'
            },
            resolve: {
              model: AdvancedFormItemResolver
            }
          }
        ]
      }
    ] 
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  providers: [
    DiningConfigurationResolver
  ]
})
export class AdminDiningRoutingModule { }
