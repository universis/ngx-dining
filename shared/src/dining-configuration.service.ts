import { Injectable } from '@angular/core';
import { Resolve, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ToastService, AppEventService, ServerEvent, ServerEventSubscriber } from '@universis/common';

@Injectable()
export class DiningConfigurationService {

}



@Injectable()
export class DiningConfigurationResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) {
   }

  resolve(): Promise<any> | any {
    return this._context.model('DiningConfigurations')
      .asQueryable()
      .orderBy('id')
      .getItems().then(res => {
        if (res) {
          return Promise.resolve(res[0]);
        }
        return Promise.resolve(null)
      });
  }

}

@Injectable()
export class DiningService {

  constructor(private _context: AngularDataContext) {
  }

  async isUpdateInProgress(event?: any): Promise<boolean> {
    if (!event) {
      event = await this._context.model('DiningRequestEvents')
      .where('eventStatus/alternateName')
      .equal('EventOpened')
      .select('id')
      .getItem();  
    }
    const latestUpdateAction = await this._context.model('UpdateDiningRequestEventActions')
         .where('object')
         .equal(event.id || event)
         .orderBy('dateModified desc')
         .take(1)
         .getItem();
  return (latestUpdateAction && latestUpdateAction.actionStatus && latestUpdateAction.actionStatus.alternateName && latestUpdateAction.actionStatus.alternateName !== 'ActiveActionStatus');
  }
}

@Injectable()
export class UpdateEventSubscriber implements ServerEventSubscriber {

  constructor(private context: AngularDataContext,
    private toastService: ToastService,
    private translateService: TranslateService,
    private appEvent: AppEventService,
    private router: Router) { }

  subscribe(event: ServerEvent): void {
    if (event == null) {
      return;
    }
    if (event.entityType === 'UpdateDiningRequestEventAction') {
      if (event.target && event.target.id) {
        this.context.model('UpdateDiningRequestEventActions')
          .where('id').equal(event.target.id)
          .select('id', 'actionResult', 'object')
          .getItem().then(updateAction => {
            if (updateAction.actionResult && updateAction.object && updateAction.object.id) {
              const urlTree = this.router.createUrlTree(
                [
                  'dining', 
                  'admin-page', 
                  'list', 
                  'results', 
                  { outlets: { modal: ['item', `${updateAction.object.id}`, 'action', `${updateAction.id}`] } }
                ]
              );
              // tslint:disable-next-line:max-line-length
              const title = this.translateService.instant(`UniversisDiningModule.DiningRequestEvent.CompletionToast.Title`, updateAction.object);
              const message = `
                    <p> ${this.translateService.instant(`UniversisDiningModule.DiningRequestEvent.CompletionToast.Message`)} </p>
                    <div>
                        <a href="#${urlTree.toString()}">${this.translateService.instant(`UniversisDiningModule.DiningRequestEvent.CompletionToast.LinkTitle`)}</a>
                    </div>
                `;
              // fire app event in case the use is already in grade submission page
              this.appEvent.change.next({
                model: 'UpdateDiningRequestEventAction',
                target: {
                  id: updateAction.id
                }
              });
              // and show toast
              return this.toastService.show(title, message, true, 30000);
            }
          });
      }
    }
  }
}

