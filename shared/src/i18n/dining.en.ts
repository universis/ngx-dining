/* tslint:disable max-line-length */
// tslint:disable: quotemark
export const en = {
  NewRequestTemplates: {
    DiningRequestAction: {
      "Name": "Dining Request",
      "Title": "Dining Requests",
      "Summary": "Dining Request",
      "Description": "Dining Requests",
      "History": "History",
      "Request": "Request"
    },
    "NewDiningRequest":"New", 
    "Settings": {
      "EditItem": "Edit"
    },
  },
  DiningPreference: {
    Title: "Interest in dining services",
    Message: {
      PREFERENCE_REQUIRED: 'The Institution provides free dining for the current academic year (application required). Are you interested?',
      REQUEST_REQUIRED: 'You have shown interest in dining services, but you have not yet submitted an application.',
      REQUEST_OPTIONAL: 'You have shown interest in dining services, but you have not yet submitted an application.'
    },
    SubmissionError: 'An error occured while saving your interest. Please try again later.',
    NotInterested: 'Not interested any more',
    NewRequest: 'Dining request'
  },
  "Settings": {
    "EditItem": "Edit"
  },
  "StudentStatuses": {
    "active": "Active",
    "candidate": "Candidate",
    "declared": "Declared",
    "erased": "Erased",
    "graduated": "Graduated",
    "null": "-",
    "suspended": "Suspended",
    "NotActive": "Not Active"
  },
  "Requests": {
    "ActionStatusTitle": "Request status",
    "StartTime": "Request date",
    "StudentUniqueIdentifier": "Student Unique Identifier",
    "StudentStatus": "Student status",
    "RequestedAfter": "Requested after",
    "RequestedBefore": "Requested before",
    "FatherName": "Father name",
    "MotherName": "Mother name",
    "RequestNumber": "Request Number",
    "GivenName": "Given name",
    "EffectiveStatus": "Evaluation status",
    "RequestCode": "Request code",
    "All": "All",
    "Current": "Current",
    "Category": "Category",
    "Categories": {
      "SpecialDiningRequestAction": "Special",
      "DiningRequestAction": ""
    },
    "AdditionalType": "Category",
    "Claimed": "Claimed", 
    "SemesterLowerOrEqual": "Semester lower or equal to"
  },
  "ActionStatusTypes": {
    "ActiveActionStatus": "Active",
    "CancelledActionStatus": "Cancelled",
    "CompletedActionStatus": "Completed",
    "FailedActionStatus": "Failed",
    "PotentialActionStatus": "Potential",
    "Unknown": " "
  },
  "UpdateActionStatusTypes": {
    "ActiveActionStatus": "Active",
    "CancelledActionStatus": "Cancelled",
    "CompletedActionStatus": "Completed",
    "FailedActionStatus": "Failed",
    "PotentialActionStatus": "Potential",
    "Unknown": " "
  },
  "EffectiveStatusTypes": {
    "RejectedAttachmentsEffectiveStatus": "Non valid documents",
    "AcceptedAttachmentsEffectiveStatus": "Valid documents",
    "UnknownStatus": "",
    "InvalidRequestDataEffectiveStatus": "Invalid request data"
  },
  "DiningRequestPreferences": {
    "Positive": "Dining preference"
  },
  "DiningRequestCategories": {
    "normal": "Normal",
    "erasmus": "Erasmus", 
    "tempi": "Special"
  },
  "DiningCardStatuses": {
    "Active": "Active",
    "Cancelled": "Cancelled",
  },
  "Cards": {
    "Current": "Current", 
    "All": "All"
  },
  "Reports": {
    "Viewer": {
      "Close": "Close",
      "Download": "Download file",
      "Print": "Print file",
      "Sign": "Sign document"
    },
  },
  "Students": {
    "FamilyName": "Family name",
    "GivenName": "Given name",
    "StudentIdentifier":"Student number",
    "StudentUniqueIdentifier": "Student identifier",
    "FullName": "Full name",
    "PersonalDetails": "Personal details",
    "StatusTypes": {
      "active": "Active",
      "candidate": "Candidate",
      "declared": "Graduated",
      "erased": "Erased",
      "graduated": "Graduated",
      "suspended": "Suspended"
    },
    "InscriptionModeCategory": "Inscription mode category",
    "Department": "Department name",
    "InscriptionYear": "Inscription year",
    "Semester": "Semester",
    "BirthDate": "Birth date",
    "BirthPlace": "Birth Place",
    "Gender": "Gender",
    "Nationality": "Citizenship",
    "VatNumber": "Vat number",
    "DepartmentAbbreviation": "Department name",
    "StudentDepartment": "Department name",
    "StudentCategory": "Student category",
    "StudentTitleSingular": "name",
    "FatherName": "Father name",
    "MotherName": "Mother name",
    "ContactDetails":"Contact Details",
    "phone": "Phone",
    "email": "email", 
    "StudyLevel": "Study level",
    "StudyLevels": {
      "undergraduate": "Undergraduate",
      "postgraduate": "Postgraduate",
      "specialprogram": "Special Program",
      "doctoral": "Doctoral",
      "postdoctoral": "Postdoctoral"
    },
  },
  "Register": {
    "Accept": "Accept",
    "Attachments": "Attachments",
    "DateCreated": "Created at",
    "DateModified": "Modified at",
    "DateSubmitted": "Submitted at",
    "Details": "Details",
    "Download": "Download",
    "Message": "Message",
    "Messages": "Messages",
    "NewMessage": "Compose new message",
    "NoAttachments": "No attachments yet",
    "NoMessages": "No messages yet",
    "Preview": "Preview",
    "Reject": "Reject",
    "Reset": "Reset application to pending",
    "Revert": "Revert to active",
    "Review": "Review",
    "TotalIncomeBasedOnStudentsRequest": "Total family income based on student's request",
    "SendMessage": "Send a message",
    "Status": "Status",
    "UploadFileHelp": "Drop file to attach, or browse",
    "UploadFilesHelp": "Drop files to attach, or browse",
    "RequestNumber": "Request number",
    "ComposeNewMessage": {
      "Title": "Compose new message",
      "Description": "The operation will try to send a message to the candidates of the selected applications. Please write a short message below and start sending messages.",
      "Subject": "Subject",
      "WriteMessage": "Write a short message",
      "Send": "Send",
      "Cancel": "Cancel",
    },
    "UserReviewAdded": "User Review Added",
    "addReview": "Add Review",
    "Edit": "Edit",
    "NoReview": "No Review",
    "Rating": "Rating",
    "CalculatedTotalFamilyIncome": "Calculated total family income" ,
    "CheckingLimit": "Checking limit",
    "NumberOfSiblingStudents": "Number of sibling students",
    "CodeTaxValue": "The field 003 value of tax clearance document",
    "TotalFamilyIncome": "Total family income",
    "RequestDetails": "Dining request details",
    "ReloadError": {
      "Title": "Refresh failed",
      "Message": "The operation has been completed successfully but something went wrong during refresh."
    } 
  },
  "Forms": {
    "DiningRequest": {
      "Disabled": "Disabled",
      "LessThan25yrs": "Less than 25yrs old",
      "LivePermanentlyInSameLocationWithInstitution": "Permanent address in same city as my Department of enrollment",
      "ForeignScholarStudent": "Foreign Scholar Student",
      "Prerequisites": "Prerequisites",
      "Unemployment": "Student Receives Unemployment Benefit",
      "StudentMaritalStatusTitle": "Marital Status",
    },
    "DiningRequestAction": "Dining Request",
    "active": "Active",
    "candidate": "Candidate",
    "declared": "Declared",
    "erased": "Erased",
    "graduated": "Graduated",
    "null": "-",
    "suspended": "Suspended",
    "transfered":"Transfered",
    "single": "Single",
    "married": "Married"
  },
  "summer": "Summer",
  "winter": "Winter",
  "AcademicPeriod": {
    "summer": "Summer",
    "winter": "Winter"
  },
  "AcademicYear": "Academic Year",
  "Attachments":{
    "Accept":"Valid documents",
    "Reject": "Non valid documents",
    "UnknownStatus": "Unknown",
    "InvalidRequestData": "Invalid request data"
  },
  "StudyLevel": "Study level",
  UniversisDiningModule: {
    DiningCardsTemplates: {
      DiningCard: {
        Name: 'Dining Card',
        Title: 'Dining Card',
        Summary: 'Dining Card',
        Description: 'Dining Cards',
      },
      validFrom: 'Valid from',
      validThrough: 'Valid through',
      SerialNumber: 'Card No',
      active: 'Dining card status',
      CancelItem: 'Suspend',
      ReactivateItem: 'Reactivate',
      AcademicYear: 'Academic year',
      AcademicPeriod: 'Academic period'
    },
    DiningRequestPreference: {
      "Name": "Dining Preference",
      "Title": "Dining Preference",
      "Summary": "Dining Preference",
      "Current": "Current",
      "All": "All", 
      "StudentHasAction": "Student has submitted an application"
    },
    DiningRequestEvent: {
      Description: "Dining periods",
      All: "All",
      Status: "Status",
      EventStatusTypes: {
        EventCancelled: "Closed",
        EventPostponed: "Postponed",
        EventRescheduled: "Rescheduled",
        EventScheduled: "Scheduled",
        EventOpened: "Opened",
        EventCompleted: "Completed"
      },
      New: "New",
      Edit: "Edit",
      Delete: "Delete",
      Extend: "Extend dining period",
      ExtensionResults: "Period actions",
      ID: "ID",
      AcademicYear: "Academic year",
      ValidFrom: "Valid from",
      ValidThrough: "Valid through",
      StartDate: "Requests open from",
      EndDate: "Requests close on",
      Actions: {
        Title: "Edit dining period",
        TitleNew: "New dining period",
        TitleEdit: "Edit dining period",
        TitleDelete: "Remove dining period",
        TitleExtend: "Extension - Edit dates",
        TitleActions: "Period extension",
        TitleActionsSucceeded: "Period extension",
        TitleActionsFailed: "Extend period actions",
        Edit: {
          SuccessTitle: "Save dining period",
          SuccessMessage: "Successfully saved dining period.",
          FailTitle: "Failed to save dining period",
          FailMessage: "The dining period could not be saved. Please try again."
        },
        Delete: {
          SuccessTitle: "Remove dining period",
          SuccessMessage: "Successfully removed the dining period.",
          FailTitle: "Failed to remove dining period",
          FailMessage: "The dining period could not be removed. Please try again."
        },
        Extend: {
          SuccessTitle: "The extension of the period has started and is currently in progress.",
          SuccessMessage: "The process of extending the period and updating the active cards has started and may take a few minutes. You will receive a notification once the process is completed.",
          FailTitle: "Failed to extend the dining event",
          FailMessage: "The dining event could not be extended. Please try again.",
          CardsMessage: "Warning! ",
          Cards:" active student dining cards found. Editing the \"Valid through\" date will try to update them.",
          EmptyCardsMessage: "During search no student dining cards were found that require updating.",
          ProcessMessage: " The process may take a few minutes." 
        },
      },
    DateCreated: "Date created",
    ValidThroughBefore: "Valid through: previous value",
    ValidThroughAfter: "Valid through: next value",
    EndDateBefore:"Requests close: previous value",
    EndDateAfter: "Requests close: next value",
    ActionStatus: "Action status",
    ActionDescription: "Action reason",
    ActionResult: "Result",
    CardsUpdated: "Cards",
    CompletionToast: {
      Title: "Extension of the dining period",
      Message: "The process of extending the period and updating the active cards has been completed. Follow the link below to see the results.",
      LinkTitle: "Results"
    },
    Id: "Period id", 
    CreatedBy: "Created by",
    DateCreatedAction: "Date created",
    FailureReasonTableTitle: "Failure reason",
    FailureReason: "Failures",
    CardsScheduled: "Cards to update",
    CardsSucceeded: "Succeeded",
    CardsFailed: "Failed",
    RequiredPrivacyPolicyConsent: "Required \"Privacy policy consent\""
  },
    DiningCardStatuses: {
      true: "Active",
      false: "Suspended",
    },
    AcademicPeriod: {
      summer: "summer",
      winter: "winter"
    },
    summer: "Summer",
    PersonalInformation: 'Personal Information',
    DiningDocuments: 'Documents',
    DiningRequestTitle: 'Dining request',
    DiningRequestCapitalTitle: 'DINING REQUEST',
    DocumentsSubmission: {
      Title: ' Documents submission',
      Subtitle: ' Follow the instructions in order to submit the required documents for your dining request.',
      SubmissionStatus: 'Submission status',
      SubmissionStatuses: {
        pending: 'Document submission for dining is ongoing',
        completed: 'Document submission for dining is completed',
        failed: 'Document submission for dining is ongoing',
        unavailable: 'Document submission is not available'
      },
      AttachmentDeleteModal: {
        Title: 'Document delete',
        Body: 'Delete document of {{attachmentType}} type?',
        Notice: 'This action can not be undone.',
        Close: 'Close',
        Delete: 'Delete'
      },
      DiningDocumentToUpload: 'document for upload',
      DiningDocumentsToUpload: 'documents for upload',
      DiningDocumentsPhysicals: 'documents to be delivered to the secretariat of the department',
      DownloadDocument: 'Download document',
      UploadDocument: 'Upload document',
      RemoveDocument: 'Remove document',
      ContactService: 'contact related service',
      Errors: {
        Download: 'There was an error during the file download',
        Remove: 'There was an error during the file removal',
        Upload: 'There was an error during the file upload'
      },
    StudentDiningCardExists: "Student Dining Card Exists", 
    },
    InvalidStudentStatus: "Υou are not entitled to cost-free dining because of your student status",
    ModalConfirm: {
      Submit: 'Submit',
      Close: 'Close',
      Title: 'Send Dining Request',
      Body: 'You want to send your request for sending and check to the secretariat?'
    },
    ModalConfirmDelete: {
      Submit: 'Delete',
      Close: 'Close',
      Title: 'Delete dining request',
      Body: 'You are about to permanently delete your dining request along with all the previously submitted data. Do you wish to proceed?'
    },
    Messages: {
      Title: 'Messages',
      NewMessage: 'New message',
      NoSubject: 'No subject',
      Subject: 'Subject',
      WriteMessage: 'Your message',
      IncomingMessage: 'Incoming message',
      NoMessages: 'No messages',
      Info:"To send supporting documents, click \"New message\" and attach the necessary documents",
      SentAfter: "Sent after",
      SentBefore: "Sent before",
      Sender: "Sender",
      Recipient: "Recipient",
      SentByStudent: "Sent by student",
      MarkAsRead: "Mark as read",
      All: "All", 
      Active: "Current"
    },
    MessagePrompt: 'Your message',
    Send: 'Send',
    Cancel: 'Cancel',
    Date: 'Date',
    Time: 'Time',
    Location: 'Location',
    Download: 'Download',
    Previous: 'Previous',
    Next: 'Next',
    Submit: 'Submit',
    Completed: 'Request Completion',
    ContactRegistrar: 'Contact Registrar',
    StudentInfo: 'Student Information',
    StudyGuide: 'STUDY GUIDE',
    Specialty: 'SPECIALTY',
    Prerequisites: 'Prerequisites',
    Progress: 'Progress',
    NoRulesFound: 'Dining Rules have not been set.',
    CourseType: 'Course Type',
    AllTypeCourses: 'All Type Courses',
    Thesis: 'Thesis',
    Student: 'Semester',
    Internship: 'Internship',
    Course: 'Prerequisite Courses',
    CourseArea: 'Courses Area',
    CourseCategory: 'Courses Category',
    CourseSector: 'Courses Sector',
    ProgramGroup: 'Courses Group',
    StatusLabel: 'Status of your request',
    NoAttachments: 'There are no attachments for upload',
    TemporarySaveMessage: 'Your dining request has been saved but not submitted yet.',
    AttachDocumentMessage: 'Please attach all required documents',
    RequestPeriodExpire1d: 'The period for dining requests has expired.',
    RequestPeriodNotStarted: 'Dining request period will be open from {{dateStart}} to {{dateEnd}}.',
    NoDiningRequestEvent: 'No dining request period defined.',
    EmptyDocumentList: "The list of required documents is empty.",
    EmptyDocumentListContinue: "The list of required documents is empty. You can continue by submitting your application.",
    VatNumberCrosscheckFailed: "Caution. The VAT check failed",
    OutΟf:"out of",
    InformationIsNotAvailable: "Information is not available",
    ModifiedBy: "Modified by",
    AcceptedAt: "Accepted at",
    "AcceptConfirm": {
      "Title": "Accept and complete",
      "Message": "You are going to accept this application. After this operation the candidate will have a new student dining card. Do you want to proceed?"
    },
    "RejectConfirm": {
      "Title": "Reject application",
      "Message": "You are going to reject this application. The candidate will be informed about application rejection. Do you want to proceed?"
    },

    "AcceptAction": {
      "Title": "Accept requests",
      "Description":"The operation will try to complete the selected requests. All the pre-defined procedures for each type of request will be performed eg document numbering or archiving. The process is valid for pending requests only."
    },
    "UnknownEffectiveStatusConfirm": {
      "Title": "Unknown state",
      "Message": "You are going to set this application's attached documents in 'Unknown' state. Do you want to proceed?"
    },
    "ResetConfirm": {
      "Title": "Change application status",
      "Message": "You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?"
    },
    "RevertConfirm": {
      "Title": "Activate application",
      "Message": "You are going to activate this application again. Do you want to proceed?"
    },
    "SuspendAction": {
      "Title": "Suspend cards",
      "Description":"The operation will try to suspend the selected dining cards. All the pre-defined procedures for each type of dining card will be performed eg document numbering or archiving. The process is valid for active dining cards only."
    },
    "ReactivateAction": {
      "Title": "Reactivate cards",
      "Description":"The operation will try to reactivate the selected dining cards. All the pre-defined procedures for each type of dining card will be performed eg document numbering or archiving. The process is valid for suspended dining cards only."
    },
    CancelCardModal: {
      "ResetConfirm": {
        "Title": "Suspend dining card",
        "Message": "You are going to suspend the dining card. If you also want to revert the dining request in 'Active' state then you have to click 'Suspend dining card and revert dining request'. The candidate will be notified by message. Do you want to proceed?"
      },
      Title: "Suspend",
      Help: "You are going to set this dining card in suspended state. If you also want to revert the dining request in 'Active' state then you have to click 'Suspend dining card and revert dining request'. The candidate will be notified by message. Do you want to proceed?",
      CancelNamePlaceholder: "Enter suspension reason",
      Accept: "Suspension",
      CancelCardActiveRequest: "Suspend and revert", 
      Close: "Close",
      TitleOnUserError: "You must specify the reason of suspension."
    },
    ReactivateCardModal:{
      "ResetConfirm": {
        "Title": "Change dining card status",
        "Message": "You are going to set this dining card in active state.  Do you want to proceed?"
       },
      Title: "Reactivate",
      Accept: "Reactivation",
      Close: "Close",
    },
    CancelCardSubject: "Suspension of dining card",
    CancelCardBody: "The dining card was suspended due: {{cancelReason}}",
    RevertCard: "Revert",
    RejectAttachmentModal: {
      Title: "Reject document",
      Help: "Specify the reason of rejection. The candidate will be notified by message.",
      RejectionNamePlaceholder: "Enter rejection reason",
      Accept: "Reject",
      Close: "Close",
      TitleOnUserError: "You must specify the reason of rejection."
    },
    RejectAttachmentSubject: "Rejection of dining request document",
    RejectAttachmentBody: "Document: '{{attachmentName}}' of your dining request was reject with reason: {{rejectionName}}",
    RevertAttachment: "Revert",
    DiningClubAttachmentNote: "An application to get free access to Dining Services must be submitted by students each year. Erasmus student applications are automatically approved. All others will need to submit up-to-date financial status and other certificates for approval according to the Ministerial Decree Φ.5/65835/Β3/18.06.2012",
    Yes: "Yes",
    No: "No",
    OutOf: "out of",
    PrintReport:"Print report", 
    DiningRequestEventStatusType: "Dining request period",
    EffectiveStatusTypes: {
      UnknownStatus: "Unknown"
    },
    EffectiveStatus: "Evaluation status",
    DiningRequestCategories: {
      tempi: "Affected by the Tempi Accident", 
      normal: "Normal", 
      erasmus: "Erasmus"
    }, 
    EffectiveStatusMessage: {
      Part1: "You are going to set this application's attached documents in:<b>",
      Part2: "</b> state. Do you want to proceed?"
    }, 
    Agent: {
      release: "Release",
      claim: "Claim action",
      NotClaimed: "This action has not be claimed by someone user. Press [Claim action] to complete this request.",
      CompletedWithErrors: {
        Title: "Completed with errors",
        Description: {
          One: "The message failed to send due to an error occurred while executing process.",
          Many: "{{errors}} messages failed to send due to an error occurred while executing process."
        }
      }, 
    },
    EffectiveStatusRating: "Effective Status Rating",
    EventStatuses: {
      NotEventOpened: "Closed",
      EventOpened: "Opened"
    }
  },
  "CreatedAt":"Created at",
  "ModifiedAt": "Modified at"
  }
