import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiningComponent } from './components/dining/dining.component';

const routes: Routes = [
  {
    path: ':action',
    component: DiningComponent,
    data: {
      model: 'DiningRequestActions'
    }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class DiningRoutingModule { }
