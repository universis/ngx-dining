import { Component, OnDestroy, AfterViewInit, OnChanges, SimpleChanges, ViewEncapsulation, ViewChild, Input, EventEmitter } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { LoadingService, ErrorService, UserService, ModalService, RequestTypesService } from '@universis/common';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import * as moment_ from 'moment';
import { TabDirective, TabsetComponent } from 'ngx-bootstrap/tabs';
import { AdvancedFormComponent } from '@universis/forms';
const moment = moment_['default'] || moment_;
import { Subscription, combineLatest } from 'rxjs';
import { ResponseError } from '@themost/client';
import { cloneDeep, template } from 'lodash';


export interface Document {
  attachments: any[];
  attachmentType: any;
  numberOfAttachments: number;
}
@Component({
  // tslint:disable-next-line: component-selector
  selector: 'app-dining',
  templateUrl: './dining.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./dining.component.scss']
})
export class DiningComponent implements AfterViewInit, OnDestroy, OnChanges {


  @ViewChild('tabset') tabset?: TabsetComponent;
  @ViewChild('formHome') formHome?: AdvancedFormComponent;
  @ViewChild('form') form?: AdvancedFormComponent;
  @ViewChild('attachDocuments') attachDocuments?: TabDirective;

  constructor(protected _context: AngularDataContext,
    protected _userService: UserService,
    protected _loading: LoadingService,
    protected _errorService: ErrorService,
    protected _activatedRoute: ActivatedRoute,
    protected _http: HttpClient,
    protected _modal: ModalService,
    protected _translateService: TranslateService,
    protected _router: Router,
    protected _requestTypes: RequestTypesService
  ) {
  }


  get documentState(): string {
    if (this.documents == null) {
      return 'invalid';
    }
    const required = this.documents.filter((item) => {
      return item.numberOfAttachments > 0 && item.attachments.length < item.numberOfAttachments;
    }).length;
    if (required > 0) {
      return 'invalid';
    }
    return 'valid';
  }
  @Input() model: any = {
    attachments: [],
    actionStatus: {
      alternateName: 'PotentialActionStatus'
    },
    effectiveStatus: null
  };
  @Input() diningRequestEvent: any;
  @Input() studentDiningCard: any;

  public queryParamSubscription?: Subscription;
  public showNewMessage = false;
  public newMessage: { subject: any; body: any; attachments: any[]; } = {
    subject: null,
    body: null,
    attachments: []
  };
  public messages: any[] = [];
  public documentStateChange: EventEmitter<any> = new EventEmitter<any>();
  public documents: Document[] = [];

  public formActionHome: any;
  public formAction: any;
  public askMoreAttachmentsForIncomeSiblings: boolean = false;
  public moreAttachmentNumberForIncomeSiblings: any;
  public askMoreAttachmentsForIncomeParents: boolean = false;
  public moreAttachmentNumberForIncomeParents: any;
  public askMoreAttachmentsForIncomeStudentChildren: boolean = false;
  public moreAttachmentNumberStudentChildren: any;
  public student: any;
  public loading = true;
  public showMessagesTab: boolean = false;
  public studentDiningRequestExists: boolean = false;
  public studentDiningCardExists: boolean = false;
  public unreadMessages: number | any;
  public validStudentStatus: boolean = false;
  public params: any;
  public selectedCategory: any;

  ngOnChanges(changes: SimpleChanges): void {
    //
  }
  ngOnDestroy(): void {
    if (this.queryParamSubscription) {
      this.queryParamSubscription.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.queryParamSubscription = combineLatest([
      this._activatedRoute.queryParams,
      this._activatedRoute.params
    ]).subscribe((results) => {
      const queryParams = results[0];
      const params = results[1];
      this.params = params;
      // track origin component 
      if (queryParams['show'] === 'messages') {
        this.showMessagesTab = true;
      }
      // validate code
      if (queryParams['code'] == null) {
        // loading
        this._loading.showLoading();
        // prepare new item
        Promise.all([
          this._userService.getUser(),
          this._context.model('students/me').asQueryable()
            .expand('user', 'department', 'studyProgram($expand=studyLevel)', 'inscriptionMode', 'person').getItem(),
          this._context.model('DiningInscriptionModeTags').asQueryable()
            .expand('inscriptionModes').getItems()
        ]).then((results1) => {
          const user = results1[0];
          this.student = results1[1];
          // check if student status is valid (active or declared) dining apply 
          this.checkStudentStatus();
          // check student inscription mode          
          const inscriptionModeTags = results1[2];
          this.checkStudentInscriptionMode(inscriptionModeTags);

          // check max time limit for free dining
          this.checkMaxTimeLimit(this.student);

          // check student vat number
          this.checkStudentVatNumber();

          // check if dining card exists
          this.checkDiningCard();

          // get dining request event
          Promise.all([
            (this._context.model('DiningRequestEvents')
              .where('eventStatus/alternateName').equal('EventOpened')
              .expand('attachmentTypes($expand=attachmentType)').getItem())
          ]).then((results2) => {
            this.diningRequestEvent = results2[0];
            if (this.diningRequestEvent) {
              this.validateRequestPeriod(this.diningRequestEvent);
              // check if student age is < 25yrs based on AcademicYear
              this.checkStudentAge(this.diningRequestEvent.academicYear);
            }
            return this._context.model(`${'DiningRequestActions'}`).where('diningRequestEvent').equal(this.diningRequestEvent.id).orderBy('dateCreated asc').expand('category').getItem().then(async (item) => {
              // check if request exists and load it
              if (item != null) {
                return this._router.navigate(['.'], {
                  queryParams: {
                    code: item.code
                  },
                  relativeTo: this._activatedRoute
                });
              }

              // find normal (plain) dining request category and initialize forms
              this._context.model('DiningRequestCategories').getItems().then((items) => {
                this._loading.showLoading();
                if (this.model) {
                  this.setDefaultBooleanValues();
                  if (this.diningRequestEvent) {
                    this.model.diningRequestEvent = this.diningRequestEvent;
                  }
                  if (this.model.mobilityProgramStudent === true) {
                    this.model.category = items.find(el =>
                      el.alternateName == 'erasmus');
                  } else {
                    this.model.category = items.find(el =>
                      el.alternateName == 'normal');
                  }
                  
                  const category = this.model.category.alternateName;
                  this.formActionHome = params['action'] ? `${'DiningRequestActions'}/home` : `${'DiningRequestActions'}/home`;
                  this.formAction = (params['action'] === 'preview') ? (this.model.mobilityProgramStudent === true ? `${'DiningRequestActions'}/${category}/edit` : `${'DiningRequestActions'}/${category}/preview`) : (this.validStudentStatus === true ? (this.model.mobilityProgramStudent === true ? `${'DiningRequestActions'}/${category}/edit` : `${'DiningRequestActions'}/${category}/edit`) : `${'DiningRequestActions'}/${category}/preview`);
                  
                  // and continue
                  this.loading = false;
                  this._loading.hideLoading();
                }
              }).catch(err => {
                console.log(err);
                return this._errorService.navigateToError(err);
              });

              // and continue
              this.loading = false;
              this._loading.hideLoading();
            });
          }).catch((err) => {
            console.log(err);
            this._loading.hideLoading();
            return this._errorService.navigateToError(err);
          });
        }).catch((err) => {
          console.log(err);
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });
      } else {
        this._loading.showLoading();
        Promise.all([
          this._context.model('students/me').asQueryable()
            .expand('user', 'department', 'studyProgram', 'inscriptionMode', 'person').getItem(),

          this._context.model(`${'DiningRequestActions'}`).where('code').equal(queryParams['code'])
          .expand('siblingFinancialAttributes','guardianFinancialAttributes','childrenFinancialAttributes','inscriptionModeTag','category','diningRequestEvent($expand=attachmentTypes($expand=attachmentType))','attachments($expand=attachmentType)','effectiveStatus($expand=locale)').getItem(),
        ]).then((results1) => {
          this.student = results1[0];
          // check if student status is valid (active or declared) dining apply 
          if (this.student) {
            this.checkStudentStatus();
          }
          const item = results1[1];
          if (item == null || this.student == null) {
            // tslint:disable-next-line:max-line-length
            return this._errorService.navigateToError(new ResponseError('The specified application cannot be found or is inaccessible', 404.5));
          }
          this.model = item;

          this.setDefaultBooleanValues();
          if (this.diningRequestEvent) {
            this.model.diningRequestEvent = this.diningRequestEvent;
          }
          if (params['action'] !== 'preview' && this.model.actionStatus.alternateName !== 'PotentialActionStatus') {
            return this._router.navigate(['..', 'preview'], {
              queryParams: {
                code: this.model.code
              },
              relativeTo: this._activatedRoute
            });
          }
          this.studentDiningRequestExists = true;

          // should have been provided for existing requests
          this.model.disableVatNumber = false;

          // check if dining card exists
          this.checkDiningCard();

          // get dining request event from model
          this.diningRequestEvent = this.model.diningRequestEvent;
          if (this.diningRequestEvent) {
            this.validateRequestPeriod(this.diningRequestEvent);
            // check if student age is < 25yrs based on AcademicYear
            this.checkStudentAge(this.diningRequestEvent.academicYear);
          }
          this.loading = false;

          // set forms based on category
          if (this.model && this.model.category) {
            const category = this.model.category && this.model.category.alternateName ? this.model.category.alternateName : 'normal';
            //set forms 
            this.setForms(params, category);
          }

          // fetch messages
          this.fetchMessages();
          // and continue
          this._loading.hideLoading();
        }).catch((err) => {
          console.log(err);
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });
      }
    });
  }

setForms(params: Params, category: string) {
  if (params['action'] === 'preview') {
    if (this.model.actionStatus && this.model.actionStatus.alternateName === 'PotentialActionStatus') {
      this._router.navigate(['..', 'apply'], {
        queryParams: {
          code: this.model.code
        },
        relativeTo: this._activatedRoute
      });
    } else {
      this.formActionHome = `${'DiningRequestActions'}/home-preview`;
      this.formAction = `${'DiningRequestActions'}/${category}/preview`
    }
  
  } else if (this.model.actionStatus && this.model.actionStatus.alternateName === 'PotentialActionStatus') {
    this.formActionHome = `${'DiningRequestActions'}/home`;
    this.formAction = `${'DiningRequestActions'}/${category}/edit`
  } 
}

setDefaultBooleanValues(){
  if  (this.model == null) {
    return;
  }
  const properties=[
    "foreignStudentOrChildOfGreekExpatriate", 
    "cyprusStudent",
    "studentDisabledPerson",
    "studentOrphan",
    "childOfVictimOfTerrorism",
    "studentSubmitsTaxReportInGreece",
    "institutionOrClubLocationSameAsStudentPermanentResidence",
    "studentReceivesUnemploymentBenefit"
  ]

  for (const prop of properties) {
    if (Object.prototype.hasOwnProperty.call(this.model, prop) && this.model[prop] == null)
      this.model[prop]= false;
  }
}


checkStudentStatus() {
    this.validStudentStatus = (this.student && this.student.studentStatus && this.student.studentStatus.alternateName === "active") ? true : false;
  }


  private checkStudentVatNumber() {
    if (this.student.person.vatNumber != null) {
      this.model.studentAfm = this.student.person.vatNumber;
      this.model.disableVatNumber = false;
    } else {
      this.model.disableVatNumber = false;
    }
  }

  private checkStudentInscriptionMode(inscriptionModeTags) {
    // no model field is defined for qualification exams prerequisite so it is declared ad-hoc with a default value
    this.model.qualificationExams = false;

    inscriptionModeTags.forEach(inscriptionModeTag => {
      // check if any of the inscription modes of the tag matches the inscription mode of the student
      // the property name of the model MUST be the same as the alternate name of the tag 
      // to set it to true or false
      if (inscriptionModeTag.inscriptionModes.find(item => item.id == this.student.inscriptionMode.id)) {
        this.model[inscriptionModeTag.alternateName] = true;
      }
      else {
        this.model[inscriptionModeTag.alternateName] = false;
      }
    });
    this.setModelValues(this.model);
  }

  private setModelValues(model: any) {
    Object.prototype.hasOwnProperty.call(model, 'ExchangeStudent') === true ? this.model.mobilityProgramStudent = this.model.ExchangeStudent : void (0);
  }

  private checkMaxTimeLimit(student) {
    // determine the maximum time limit in semesters for active students who attend a study program
    // and their current semester is known. Return true if the student is beyond the maximum time 
    // limit for free dining, false otherwise.
    this.model.maxTimeLimit = true;

    if (student.studentStatus.alternateName === 'active') {
      if (student.studyProgram !== null && student.semester !== null) {
        let maxSemestersLimit = 0;
        const studySemesters = student.studyProgram.semesters;
        switch (student.studyProgram.studyLevel.alternateName) {
          case 'undergraduate': maxSemestersLimit = studySemesters + 4; break;
          case 'postgraduate': maxSemestersLimit = studySemesters; break;
          case 'doctoral': maxSemestersLimit = 8; break;
          default: maxSemestersLimit = 0; break;
        }
        this.model.maxTimeLimit = student.semester > maxSemestersLimit;
      }
    }
  }

  private checkStudentAge(year: any) {
    const academicYear = year.id || year;
    if (this.student.person.birthDate != null) {
      const birthDate = moment(this.student.person.birthDate, 'YYYY-MM-DD');
      const age = moment(new Date(academicYear, 8, 1)).diff(birthDate, 'years');
      if (age < 25) {
        this.model.lessThan25yrs = true;
      } else {
        this.model.lessThan25yrs = false;
      }
      this.model.disableLessThan25yrsField = true;
    } else {
      this.model.disableLessThan25yrsField = false;
    }
  }

  checkDiningCard(): void {
    const academicYear = this.model && this.model.diningRequestEvent && (this.model.diningRequestEvent.academicYear.id || this.model.diningRequestEvent.academicYear);
          // check if student dining card exists
    this._context.model('StudentDiningCards')
    .where('student').equal(this.student.id)
    .and('academicYear').equal(academicYear)
    .and('action/diningRequestEvent').equal(this.model && this.model.diningRequestEvent && this.model.diningRequestEvent.id)
    // TO DO add academic period on query when specs are final

    .getItem().then((card) => {
      if (card != null) {
        this.studentDiningCard = card;
        this.studentDiningCardExists = true;
        if (this.model.studentDiningCard && this.studentDiningCard.length === 0) {
          this.studentDiningCardExists = false;
        }
      }
    }).catch((err) => {
      console.log(err);
      window.location.reload();
      this._loading.hideLoading();
    });
  }

  fetchMessages(): void {
    if (this.model && this.model.id) {
      this.loading = true;
      this._context.model(`${'DiningRequestActions'}/${this.model.id}/messages`).asQueryable()
        .orderBy('dateCreated desc').expand('attachments').getItems().then((messages) => {
          this.messages = messages;
          (this.messages || []).map((message) => {
            message.dateReceived == null ? message.read = false : message.read = true;
            return message;
          });
          this.unreadMessages = this.messages.filter((read) => read.dateReceived == null).length;
          this.loading = false;
        }).catch((err) => {
          console.log(err);
          this.messages = [];
          this.loading = false;
        });
    } else {
      this.messages = [];
    }
  }


  validateRequestPeriod(event: { startDate?: Date; endDate?: Date, validFrom?: Date, validThrough?: Date }): void {
    const now = new Date();
    let valid = false; // event period
    let started = false; // request period
    if (event.startDate instanceof Date) {
      started = event.startDate <= now && event.endDate > now;
    } 
    if (event.validFrom instanceof Date) {
      valid = event.validFrom <= now && event.validThrough > now;
    } 
    Object.assign(event, {
      valid,
      started
    });
  }

  onFileSelect(event, attachmentType): any {
    // get file
    const addedFile = event.addedFiles[0];
    const formData: FormData = new FormData();
    formData.append('file', addedFile, addedFile.name);
    formData.append('attachmentType', attachmentType.id);
    formData.append('published', 'true');
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`${'DiningRequestActions'}/${this.model.id}/addAttachment`);
    this._loading.showLoading();
    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    }).subscribe((result) => {
      // reload attachments
      this._context.model(`${'DiningRequestActions'}/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          console.log(err);
          window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      console.log(err);
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onFileRemove(attachment: any): any {
    this._loading.showLoading();
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`${'DiningRequestActions'}/${this.model.id}/removeAttachment`);
    return this._http.post(postUrl, attachment, {
      headers: serviceHeaders
    }).subscribe(() => {
      // reload attachments
      this._context.model(`${'DiningRequestActions'}/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          console.log(err);
          window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      console.log(err);
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onSelectAttachDocuments(): void {
    this.documents = this.documents || [];
    // if study program enrollment event is not defined
    if (this.diningRequestEvent == null) {
      // exit
      return;
    }
    // if guardianType is parents then ask financial data for both
    if (this.model.guardianType) {
      this.askMoreAttachmentsForIncomeParents = false;
      if (this.model.guardianType.alternateName === 'parents' && this.model.studentParentsFileJointTaxReport === false) {
        this.askMoreAttachmentsForIncomeParents = true;
        this.moreAttachmentNumberForIncomeParents = this.model.guardianFinancialAttributes.length;
      }
    }
    // if studentSiblings file tax report then ask financial documents
    if (this.model.maritalStatus && this.model.guardianNoOfChildrenLessThan18yrs) {
      this.askMoreAttachmentsForIncomeSiblings = false;
      if (this.model.maritalStatus.alternateName === 'single' && this.model.guardianNoOfChildrenLessThan18yrs > 0) {
        this.askMoreAttachmentsForIncomeSiblings = true;
        this.moreAttachmentNumberForIncomeSiblings = this.model.siblingFinancialAttributes.filter((el) => el.hasTaxDeclaration === true).length;
      }
    }

    // if studentChildren file tax report then ask financial documents
    if (this.model.maritalStatus && this.model.noOfChildren) {
      this.askMoreAttachmentsForIncomeStudentChildren = false;
      if (this.model.maritalStatus.alternateName === 'married' && this.model.noOfChildren !== 0) {
        this.askMoreAttachmentsForIncomeStudentChildren = true;
        this.moreAttachmentNumberStudentChildren = this.model.childrenFinancialAttributes.filter((el) => el.hasTaxDeclaration === true).length;
      }
    }

    this.documents = this.diningRequestEvent.attachmentTypes.map((item: any) => {

      const res = {
        attachmentType: item.attachmentType,
        numberOfAttachments: this.checkNumberOfAttachments(item) ? this.checkNumberOfAttachments(item) : item.numberOfAttachmentsProperty !== null ? this.model[item.numberOfAttachmentsProperty] : item.numberOfAttachments,
        requiredRule: item.requiredRule
      };

      // try to search model attachments list
      if (this.model.attachments && Array.isArray(this.model.attachments)) {
        const findAttachments = this.model.attachments.filter((attachment) => {
          if (attachment.attachmentType == null || attachment.published === false) {
            return false;
          }
          return attachment.attachmentType.id === item.attachmentType.id;
        });

        Object.assign(res, {
          attachments: findAttachments
        });
        return res;
      }

    }).filter(item => {

      // console.log(item.attachmentType.alternateName + ' ' + item.requiredRule);

      if (item.requiredRule) {

        // determine if the attachment is required or not
        try {
          const result: string = template('${' + item.requiredRule + '}')(this.model);
          const numberOfAttachments = result === 'true' ? item.numberOfAttachments : 0;
          if (numberOfAttachments === 0) {
            return false;
          }
          return true;
        } catch (err) {
          console.log(err);
          return false;
        }
      }
      return false;
    });
    this.documentStateChange.emit(this.documentState);
  }

  submit(): any {
    const SubmitTitle = this._translateService.instant('UniversisDiningModule.ModalConfirm.Title');
    const SubmitTitleMessage = this._translateService.instant('UniversisDiningModule.ModalConfirm.Body');
    this._modal.showSuccessDialog(SubmitTitle, SubmitTitleMessage).then((result) => {
      if (result === 'ok') {
        this._loading.showLoading();
        const model = cloneDeep(this.model);
        // remove attachments
        delete model.attachments;
        model.effectiveStatus = null;
        // set action status
        model.actionStatus = {
          alternateName: 'ActiveActionStatus'
        };
        this._context.model(`${'DiningRequestActions'}`).save(model).then(() => {
          // navigate to list
          this._loading.hideLoading();
          // requests/list
          return this._router.navigate(['requests/list']);
        }).catch((err) => {
          console.log(err);
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  downloadAttachment(attachment: any): void {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loading.showLoading();
    fetch(fileURL, {
      headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status !== 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loading.hideLoading();
      }).catch((err) => {
        console.log(err);
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }


  async send(message: any): Promise<void> {
    try {
      this._loading.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        student: this.student.id,
        recipient: 'DiningUsers'
      });
      const formData: FormData = new FormData();
      // get attachment if any
      if (message.attachments && message.attachments.length) {
        formData.append('attachment', message.attachments[0], message.attachments[0].name);
      }
      Object.keys(message).filter((key) => {
        return key !== 'attachments';
      }).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(message, key)) {
          formData.append(key, message[key]);
        }
      });
      // get context service headers
      const serviceHeaders = this._context.getService().getHeaders();
      const serviceUrl = this._context.getService().resolve(`${'DiningRequestActions'}/${this.model.id}/sendMessage`);
      await this._http.post(serviceUrl, formData, {
        headers: serviceHeaders
      }).toPromise();
      // reload message
      this.fetchMessages();
      // clear message
      this.newMessage = {
        subject: null,
        body: null,
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      console.log(err);
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  checkNumberOfAttachments(item: any) {
    var numberOfAttachments;
    // student not Cyprus or Foreign
    if (this.model.cyprusStudent !== true && this.model.foreignStudentOrChildOfGreekExpatriate !== true) {
      switch (item.attachmentType.alternateName) {
        case 'studentParentsTaxIncomeDocument':
        case 'studentParentsTaxE1Document':
          if (this.askMoreAttachmentsForIncomeParents === true) {
            numberOfAttachments = this.moreAttachmentNumberForIncomeParents;
          }
          break;
        case 'studentChildrenTaxIncomeDocument':
        case 'studentChildrenTaxE1Document':
          if (this.askMoreAttachmentsForIncomeStudentChildren === true) {
            numberOfAttachments = this.moreAttachmentNumberStudentChildren;
          }
          break;
        case 'studentSiblingTaxIncomeDocument':
        case 'studentSiblingTaxE1Document':
          if (this.askMoreAttachmentsForIncomeSiblings === true) {
            numberOfAttachments = this.moreAttachmentNumberForIncomeSiblings;
          }
          break;
      }
    }

    // Cyprus student
    if (this.model.cyprusStudent === true && this.model.foreignStudentOrChildOfGreekExpatriate !== true) {
      switch (item.attachmentType.alternateName) {
        case 'familyCyprusIncomeCertificate':
          if (this.askMoreAttachmentsForIncomeParents === true && this.model.cyprusStudent == true && this.model.foreignStudentOrChildOfGreekExpatriate !== true) {
            numberOfAttachments = this.moreAttachmentNumberForIncomeParents;
          }
          break;
        case 'siblingsCyprusIncomeCertificate':
          if (this.askMoreAttachmentsForIncomeSiblings === true) {
            numberOfAttachments = this.moreAttachmentNumberForIncomeSiblings;
          }
          break;
        case 'studentChildrenCyprusIncomeCertificate':
          if (this.askMoreAttachmentsForIncomeSiblings === true) {
            numberOfAttachments = this.moreAttachmentNumberStudentChildren;
          }
          break;
      }
    }

    // foreign Student Or Child Of Greek Expatriate
    if (this.model.cyprusStudent !== true && this.model.foreignStudentOrChildOfGreekExpatriate === true) {
      switch (item.attachmentType.alternateName) {
        case 'foreignFamilyIncomeCertificate':
          if (this.askMoreAttachmentsForIncomeParents === true && this.model.cyprusStudent !== true && this.model.foreignStudentOrChildOfGreekExpatriate === true) {
            numberOfAttachments = this.moreAttachmentNumberForIncomeParents;
          }
          break;
        case 'studentChildrenForeignIncomeCertificate':
          if (this.askMoreAttachmentsForIncomeStudentChildren === true) {
            numberOfAttachments = this.moreAttachmentNumberStudentChildren;
          }
          break;
        case 'studentSiblingForeignIncomeCertificate':
          if (this.askMoreAttachmentsForIncomeSiblings === true) {
            numberOfAttachments = this.moreAttachmentNumberForIncomeSiblings;
          }
          break;
      }
    }
    return numberOfAttachments;
  }


  onDeleteRequest(request) {
    const SubmitTitle = this._translateService.instant('UniversisDiningModule.ModalConfirmDelete.Title');
    const SubmitTitleMessage = this._translateService.instant('UniversisDiningModule.ModalConfirmDelete.Body');
    this._modal.showWarningDialog(SubmitTitle, SubmitTitleMessage).then((result) => {
      if (result === 'ok') {

        this._loading.showLoading();
        this._context.model(`${'DiningRequestActions'}/${this.model.id}/Cancel`).save(this.model).then(() => {
          // navigate to list
          this._loading.hideLoading();
          // requests/list
          return this._router.navigate(['requests/list']);
        }).catch((err) => {
          console.log(err);
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  onSelectTab(event: any): void {
    switch (event.id) {
      case 'attach-documents':
        this.onSelectAttachDocuments();
        break;
      case 'personal-information':
        if (this.model.actionStatus.alternateName === 'PotentialActionStatus' && this.validStudentStatus === true) {
          const find = this.tabset?.tabs.find((tab) => {
            return tab.id === 'attach-documents';
          });
          if (find) {
            find.disabled = true;
          }
        }
        break;
      case 'home':
        if (this.model.actionStatus.alternateName === 'PotentialActionStatus' && this.validStudentStatus === true) {
          const find = this.tabset?.tabs.find((tab) => {
            return tab.id === 'attach-documents';
          });
          if (find) {
            find.disabled = true;
          }
          const find2 = this.tabset?.tabs.find((tab) => {
            return tab.id === 'personal-information';
          });
          if (find2) {
            find2.disabled = true;
          }
          this.formAction = '';
        }
        break;
      case 'messages':
        break;
    }
  }

  onDataChange(event: any): void {
    //
  }

  onDataChangeHome(event: any): void {
    if (event.type === 'change') {
      if (this.form && this.formHome?.form?.formio?.data?.category && (typeof this.formHome?.form?.formio?.data?.category === 'object')) {
          const category = this.formHome?.form?.formio?.data?.category;
          this.form.ngOnDestroy();
          this.form.formName = `${'DiningRequestActions'}/${category.alternateName}/edit`;
          this.form.ngOnInit();
        } else if (this.formHome?.form?.formio?.data && typeof this.formHome?.form?.formio?.data?.category !== 'object'){
          this.formHome.form.formio.data.category = null;
          this.form.ngOnDestroy();
          this.form.ngOnInit();
        }
    }
  }

  next(): void {
    // get active tab
    if (this.tabset) {
      const findIndex = this.tabset.tabs.findIndex((tab) => {
        return tab.active;
      });
      if (findIndex < this.tabset.tabs.length) {
        // set active tab
        const tab = this.tabset.tabs[findIndex + 1];
          this.beforeNext(tab).then((result) => {
            if (result) {
              tab.disabled = false;
              tab.active = true;
            }
          });
      }
    }
  }

  previous(): void {
    if (this.tabset) {
      const findIndex = this.tabset.tabs.findIndex((tab) => {
        return tab.active;
      });
      if (findIndex < this.tabset.tabs.length) {
        const tab = this.tabset.tabs[findIndex - 1];
        this.beforeNext(tab).then((result) => {
          if (result) {
            tab.disabled = false;
            tab.active = true;
          }
        });
      }
    }
  }

  onCustomEvent(event: any): void {
    if (event.type === 'nextEvent') {
      event.data.category = this.formHome?.form?.formio?.data?.category;
      this.next();
    }
  }


  async beforeNext(nextTab: TabDirective): Promise<boolean> {
    try {
      // save application before attach documents
      if (nextTab.id === 'attach-documents') {
        // save action
        if (this.model.actionStatus.alternateName === 'PotentialActionStatus') {
          this._loading.showLoading();
          // set additional data
          const data = this.form?.form?.formio?.data;

          //TO BE REMOVED
          if (!data.studentOrphan) {
            data.studentOrphan = false;
          }
          if (!data.maritalStatus) {
            data.maritalStatus = null;
          }
          if (!data.numberOfStudyingSiblings) {
            data.numberOfStudyingSiblings = null;
          }
          if (Object.prototype.hasOwnProperty.call(data, 'studentGuardianSubmitsTaxReportInGreece') === false) {
            data.studentGuardianSubmitsTaxReportInGreece = null;
          }
          if (Object.prototype.hasOwnProperty.call(data, 'siblingFinancialAttributes') === false) {
            data.siblingFinancialAttributes = [];
          }
          if (Object.prototype.hasOwnProperty.call(data, 'childrenFinancialAttributes') === false) {
            data.childrenFinancialAttributes = [];
          }
          if (Object.prototype.hasOwnProperty.call(data, 'guardianFinancialAttributes') === false) {
            data.guardianFinancialAttributes = [];
          }
          if (Object.prototype.hasOwnProperty.call(data, 'noOfChildren') === false) {
            data.noOfChildren = 0;
          }
          if (Object.prototype.hasOwnProperty.call(data, 'url') === false || data.url == null) {
            data.url = this._router.url;
          }
          Object.assign(data, {
            agent: null,
            diningRequestEvent: this.diningRequestEvent
          });

          // save application
          const result = await this._context.model('DiningRequestActions').save(data);
          // update current action
          Object.assign(this.model, result);
          // and continue
          this._loading.hideLoading();
        }
      }
      return true;
    } catch (err) {
      console.log(err);
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
      return false;
    }
  }

}
