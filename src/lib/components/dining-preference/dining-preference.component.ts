import { Component, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import {
  ConfigurationService,
  ErrorService,
  LoadingService,
} from '@universis/common';

@Component({
  selector: 'universis-dining-preference',
  templateUrl: './dining-preference.component.html',
  styleUrls: ['./dining-preference.component.scss'],
})
export class DiningPreferenceComponent implements OnInit {
  public diningPreferenceEffectiveStatus: any;
  public currentLanguage: string;
  public diningRequestConfiguration: any;

  @Input() title: string = 'DiningPreference.Title';
  @Input() icon: string = 'fas fa-utensils';
  @Input() message: string = '';
  @Input() extraMessage: string = '';

  constructor(
    private readonly _context: AngularDataContext,
    private readonly _errorService: ErrorService,
    private readonly _configurationService: ConfigurationService,
    private readonly _loadingService: LoadingService,
    private readonly _translateService: TranslateService
  ) {
    // get current language
    this.currentLanguage = this._configurationService.currentLocale;
  }

  async ngOnInit(): Promise<void> {
    try {
      await this.loadComponentData();
    } catch (err) {
      console.error(err);
    }
  }

  private async loadComponentData() {
    // get dining preference effective status
    this.diningPreferenceEffectiveStatus = await this._context
      .model('students/me/diningPreferenceEffectiveStatus')
      .asQueryable()
      .getItem();
    if (this.diningPreferenceEffectiveStatus == null) {
      return;
    }
    // prepare message according to code
    this.message = `DiningPreference.Message.${this.diningPreferenceEffectiveStatus.code}`;
    // add description to extra message
    this.extraMessage =
      (this.diningPreferenceEffectiveStatus.locale &&
        this.diningPreferenceEffectiveStatus.locale.description) ||
      this.diningPreferenceEffectiveStatus.description;
    // if the student should create a dining request
    if (
      this.diningPreferenceEffectiveStatus.code === 'REQUEST_REQUIRED' ||
      this.diningPreferenceEffectiveStatus.code === 'REQUEST_OPTIONAL'
    ) {
      // get dining request configuration to find navigation url
      this.diningRequestConfiguration = await this._context
        .model('StudentRequestConfigurations')
        .where('additionalType')
        .equal('DiningRequestAction')
        .select('url')
        .getItem();
    }
  }

  async submitDiningPreference(preference: boolean, action: string) {
    try {
      if (
        !(
          typeof preference === 'boolean' &&
          this.diningPreferenceEffectiveStatus &&
          ((action === 'create' &&
            this.diningPreferenceEffectiveStatus.diningRequestEvent) ||
            (action === 'update' &&
              this.diningPreferenceEffectiveStatus.diningRequestPreference))
        )
      ) {
        throw new Error(
          this._translateService.instant('DiningPreference.SubmissionError')
        );
      }
      this._loadingService.showLoading();
      // prepare dining preference object according to action
      const diningPreference =
        action === 'create'
          ? {
              preference,
              diningRequestEvent:
                this.diningPreferenceEffectiveStatus.diningRequestEvent,
            }
          : {
              id:
                this.diningPreferenceEffectiveStatus.diningRequestPreference
                  .id ||
                this.diningPreferenceEffectiveStatus.diningRequestPreference,
              preference,
            };
      // create or update the preference
      await this._context
        .model('DiningRequestPreferences')
        .save(diningPreference);
      // and refresh data
      await this.loadComponentData();
    } catch (err) {
      console.error(err);
      this._errorService.showError(err, {
        continueLink: '.',
      });
    } finally {
      this._loadingService.hideLoading();
    }
  }
}
