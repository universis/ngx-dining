/*
 * Public API Surface of dining
 */

export * from './lib/components/dining/dining.component';
export * from './lib/dining.module';
export * from './lib/dining-routing.module';
export * from './lib/components/send-message-action/send-message-action.component';
export * from './lib/components/dining-preference/dining-preference.component';
